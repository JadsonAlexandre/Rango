package br.com.rango.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.apache.commons.validator.routines.EmailValidator;

@FacesValidator("emailValidator")
public class EmailValidador implements Validator {

	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		if(value!=null && value.toString().length()>0) {
			if(!EmailValidator.getInstance().isValid(value.toString())){
				FacesMessage msg = new FacesMessage("Email inválido!", "Insira seu email corretamente.");
				msg.setSeverity(FacesMessage.SEVERITY_ERROR);
				throw new ValidatorException(msg);						
			}
		}

	}

}
