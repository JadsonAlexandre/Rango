package br.com.rango.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import br.com.rango.util.CPFUtil;

@FacesValidator("cpfValidator")
public class CPFValidator implements Validator {

	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		if(value!=null) {

			if(!CPFUtil.isValid(value.toString())) {
				FacesMessage msg = new FacesMessage("CPF inválido!", "Insira seu CPF corretamente.");
				msg.setSeverity(FacesMessage.SEVERITY_ERROR);
				throw new ValidatorException(msg);
			}
		}

	}

}
