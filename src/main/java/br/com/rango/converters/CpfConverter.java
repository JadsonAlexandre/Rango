package br.com.rango.converters;

import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

@FacesConverter(value="cpfConverter")
public class CpfConverter implements Converter {
	
	private Pattern pattern = Pattern.compile("^\\d{3}\\x2E\\d{3}\\x2E\\d{3}\\x2D\\d{2}$");

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		
		if (value == null || (value.trim().length() == 0)) {
			return null;
		}

		// Verificar se a string está no formato esperado. Ex: 103.297.484-24
		if (!pattern.matcher(value).matches()) {
			String msgErroStr = String.format(
					"CPF Inválido! Seu CPF deve conter 11 dígitos.",
					value);
			FacesMessage msgErro = new FacesMessage(FacesMessage.SEVERITY_ERROR, msgErroStr, msgErroStr);
			throw new ConverterException(msgErro);
		}
		
		String retorno = value.substring(0, 3);
		retorno += value.substring(4, 7);
		retorno += value.substring(8, 11);
		retorno += value.substring(12, 14);
		
		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (!(value instanceof String)) {
			return null;
		}
		
		String cpf = value.toString();	
		if(cpf.length()!=11) {
			return null;
		}
		
		String retorno = cpf.substring(0, 3);
		retorno+=".";
		retorno+= cpf.substring(3, 6);
		retorno+=".";
		retorno+= cpf.substring(6, 9);
		retorno+="-";
		retorno+= cpf.substring(9, 11);
		
		return retorno;
	}

}
