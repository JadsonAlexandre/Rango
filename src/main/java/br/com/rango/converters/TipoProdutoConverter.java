package br.com.rango.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import br.com.rango.entities.TipoProduto;
import br.com.rango.service.TipoProdutoService;

@FacesConverter(value = "tipoProdutoConverter")
public class TipoProdutoConverter implements Converter {
	
	@Inject
	TipoProdutoService service;
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value == null || value.trim().isEmpty()) {
			return null;
		}
		
		try{
			Long id = Long.valueOf(value);
			return service.buscarPorID(id);
			
		}catch (ConverterException e) {
			throw e;
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value == null) {
			return null;
		}
		Long id = ((TipoProduto) value).getId();
		return (id != null) ? id.toString() : null;
	}

}
