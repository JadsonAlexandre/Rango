package br.com.rango.converters;

import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

@FacesConverter(value="telefoneConverter")
public class TelefoneConverter implements Converter {
	
	private Pattern pattern = Pattern.compile("^\\(?\\d{2}\\)?[\\s-]?\\d{5}-?\\d{4}$");

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value == null || (value.trim().length() == 0)) {
			return null;
		}

		// Verificar se a string está no formato esperado. Ex: (83)99804-0556
		if (!pattern.matcher(value).matches()) {
			String msgErroStr = String.format(
					"Telefone Inválido! Seu telefone deve conter 11 dígitos.",
					value);
			FacesMessage msgErro = new FacesMessage(FacesMessage.SEVERITY_ERROR, msgErroStr, msgErroStr);
			throw new ConverterException(msgErro);
		}
		
		String retorno = value.substring(1,3);
		retorno += value.substring(5, 10);
		retorno += value.substring(11, 15);
		
		return Long.valueOf(retorno);
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (!(value instanceof Long)) { 
			return null;
		}
		
		String telefone = value.toString();	
		if(telefone.length()!=11) {
			return null;
		}
		
		String retorno = "(";		
		retorno+= telefone.substring(0, 2);
		retorno+=") ";
		retorno+= telefone.substring(2, 7);
		retorno+="-";
		retorno+= telefone.substring(7, 11);
		
		return retorno;
	}

}
