package br.com.rango.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter(value="localizacaoConverter")
public class LocalizacaoConverter implements Converter {
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value == null || (value.trim().length() == 0)) {
			return null;
		}
		
		if(value.toString().toLowerCase().equalsIgnoreCase("externa")) {
			return true;
		}
		
		else if(value.toString().toLowerCase().equalsIgnoreCase("interna")) {
			return false;
		}
		
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (!(value instanceof Boolean)) {
			return null;
		}
		boolean valor = (Boolean) value;
		String retorno = null;
		
		if(valor==true) {
			retorno = "Externa";			
		}
		
		if(valor==false) {
			retorno = "Interna";			
		}		
		
		return retorno;
	}

}
