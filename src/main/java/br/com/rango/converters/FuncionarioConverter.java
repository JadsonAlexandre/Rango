package br.com.rango.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import br.com.rango.entities.Funcionario;
import br.com.rango.service.FuncionarioService;

@FacesConverter(value = "funcionarioConverter")
public class FuncionarioConverter implements Converter {
	
	@Inject
	FuncionarioService service;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value == null || value.trim().isEmpty()) {
			return null;
		}
		
		try{
			return service.buscarFuncionarioLogin(value.toString());
			
		}catch (ConverterException e) {
			throw e;
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value == null) {
			return null;
		}
		Long id = ((Funcionario) value).getIdFuncionario();

		return (id != null) ? id.toString() : null;
	}

}
