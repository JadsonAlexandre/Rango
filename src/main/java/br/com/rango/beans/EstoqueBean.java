package br.com.rango.beans;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import br.com.rango.entities.Estabelecimento;
import br.com.rango.entities.Produto;
import br.com.rango.entities.TipoProduto;
import br.com.rango.service.ProdutoService;
import br.com.rango.service.TipoProdutoService;

@Named(value="estoqueBean")
@ViewScoped
public class EstoqueBean extends AbstractBean {

	private static final long serialVersionUID = -3357913908084405715L;

	@Inject
	private TipoProdutoService tipoProdutoService;

	@Inject
	private ProdutoService produtoService;

	private TipoProduto tipoProduto;
	private TipoProduto tipoProdutoSelecionado;
	private List<TipoProduto> tiposProduto;

	private Produto produto;
	private Produto produtoSelecionado;
	private List<Produto> produtos;

	private Estabelecimento estabelecimento;

	@PostConstruct
	public void init () {		
		//PEGANDO ESTABELECIMENTO	
		FacesContext fc = FacesContext.getCurrentInstance();		
		estabelecimento = (Estabelecimento) fc.getExternalContext().getSessionMap().get("estabelecimento");		
		limpar();
	}

	private void limpar() {
		tipoProduto = new TipoProduto();
		produto = new Produto();
		try {			
			tiposProduto = (List<TipoProduto>) tipoProdutoService.buscarTodosTipos(estabelecimento);
			produtos = (List<Produto>) produtoService.buscarTodosProdutos(estabelecimento);
		} catch (Exception e) {
			notificarMensagemDeErro(e.getMessage());
		}
	}
	
	public void cadastrarTipoProduto() {		
		try {					
			tipoProduto.setEstabelecimento(estabelecimento);
			String mensagem = tipoProdutoService.cadastrarTipo(tipoProduto);
			notificarMensagemDeSucesso(mensagem);
			limpar();
		} catch (Exception e) {
			reportarMensagemDeErro(e.getMessage());
		}
	}

	public void cadastrarProduto() {		
		try {		
			validarProduto(produto);			
			produto.setEstabelecimento(estabelecimento);
			String mensagem = produtoService.cadastrarProduto(produto);
			notificarMensagemDeSucesso(mensagem);
			limpar();
		} catch (Exception e) {
			reportarMensagemDeErro(e.getMessage());
		}

	}

	private void validarProduto(Produto produto) throws Exception {
		if(produto.getValorVenda().doubleValue()<=produto.getValorCusto().doubleValue()) {
			throw new Exception("O valor de venda deve ser maior que o de compra");
		}
		if(produto.getQuantidade()!=null && produto.getQuantidade()==0) {
			throw new Exception("A quantidade deve ser maior que 0");
		}
	}

	public void atualizarTipoProduto() {
		try {
			String mensagem = tipoProdutoService.atualizarTipo(tipoProdutoSelecionado);
			notificarMensagemDeSucesso(mensagem);
			esconderModal("modalEditarCategoria");
			limpar();
		} catch (Exception e) {
			reportarMensagemDeErro(e.getMessage());
		}		
	}

	public void atualizarProduto() {
		try {
			validarProduto(produtoSelecionado);
			String mensagem = produtoService.atualizarProduto(produtoSelecionado);
			notificarMensagemDeSucesso(mensagem);
			esconderModal("modalEditarProduto");			
			limpar();
		} catch (Exception e) {
			reportarMensagemDeErro(e.getMessage());
		}		
	}

	public void excluirTipoProduto() {
		try {
			String mensagem = tipoProdutoService.removerTipo(tipoProdutoSelecionado);
			notificarMensagemDeSucesso(mensagem);			
			limpar();
		} catch (Exception e) {
			notificarMensagemDeErro(e.getMessage());
		}			
	}

	public void excluirProduto() {
		try {
			String mensagem = produtoService.removerProduto(produtoSelecionado);
			notificarMensagemDeSucesso(mensagem);			
			limpar();
		} catch (Exception e) {
			reportarMensagemDeErro(e.getMessage());
		}			
	}

	public int produtosEmFalta() {
		int i = 0;
		for(Produto p: produtos) {
			if(!p.isDaCasa() && p.getQuantidade()==0) {
				i++;
			}
		}
		return i;
	}
	
	public void filtrarItens() {
		try {
			if(tipoProdutoSelecionado==null) {
				produtos =(List<Produto>) produtoService.buscarTodosProdutos(estabelecimento);
			}
			else {
				produtos = (List<Produto>) produtoService.buscarTodosPorTipo(tipoProdutoSelecionado);										
			}
		} catch (Exception e) {
			notificarMensagemDeErro(e.getMessage());
		}
	}

	//GETTERS AND SETTERS
	public TipoProduto getTipoProduto() {
		return tipoProduto;
	}
	public void setTipoProduto(TipoProduto tipoProduto) {
		this.tipoProduto = tipoProduto;
	}
	public TipoProduto getTipoProdutoSelecionado() {
		return tipoProdutoSelecionado;
	}
	public void setTipoProdutoSelecionado(TipoProduto tipoProdutoSelecionado) {
		this.tipoProdutoSelecionado = tipoProdutoSelecionado.clone();
	}
	public List<TipoProduto> getTiposProduto() {
		return tiposProduto;
	}
	public void setTiposProduto(List<TipoProduto> tiposProduto) {
		this.tiposProduto = tiposProduto;
	}
	public Produto getProduto() {
		return produto;
	}
	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	public Produto getProdutoSelecionado() {
		return produtoSelecionado;
	}
	public void setProdutoSelecionado(Produto produtoSelecionado) {
		this.produtoSelecionado = produtoSelecionado.clone();
	}
	public List<Produto> getProdutos() {
		return produtos;
	}
	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}
	public Estabelecimento getEstabelecimento() {
		return estabelecimento;
	}
	public void setEstabelecimento(Estabelecimento estabelecimento) {
		this.estabelecimento = estabelecimento;
	}	
}
