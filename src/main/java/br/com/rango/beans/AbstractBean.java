package br.com.rango.beans;

import java.io.Serializable;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;
import org.primefaces.context.RequestContext;
import br.com.rango.entities.FormaPagamento;
import br.com.rango.entities.Plano;
import br.com.rango.entities.TipoFuncionario;

public abstract class AbstractBean implements Serializable {

	private static final long serialVersionUID = 7887186144461468149L;
	
	protected void esconderModal(String id) {
		RequestContext.getCurrentInstance().execute("$('#"+id+"').modal('hide');");	
	}

	private void notificarMensagem(String mensagem, String tipo) {
		RequestContext.getCurrentInstance().execute("demo.showNotification('top','center','"+mensagem+"','"+ tipo+"');");
	}

	protected void notificarMensagemDeSucesso(String mensagem) {
		notificarMensagem(mensagem, "success");
	}

	protected void notificarMensagemDeErro(String mensagem) {
		notificarMensagem(mensagem, "danger");
	}

	protected void reportarMensagemDeErro(String detalhe) {
		reportarMensagem(true, detalhe);
	}

	protected void reportarMensagemDeSucesso(String detalhe) {
		reportarMensagem(false, detalhe);
	}

	private void reportarMensagem(boolean isErro, String detalhe) {
		String tipo = "";
		Severity severity = FacesMessage.SEVERITY_INFO;
		if (isErro) {
			tipo = "";
			severity = FacesMessage.SEVERITY_ERROR;
			FacesContext.getCurrentInstance().validationFailed();
		}

		FacesMessage msg = new FacesMessage(severity, tipo + " " + detalhe, null);

		Flash flash = FacesContext.getCurrentInstance().getExternalContext().getFlash();
		flash.setKeepMessages(true);
		flash.setRedirect(true);
		FacesContext.getCurrentInstance().addMessage(null, msg);

	}

	public boolean isUserInRole(String role) {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ExternalContext externalContext = facesContext.getExternalContext();
		return externalContext.isUserInRole(role);
	}

	public String getUserLogin() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ExternalContext externalContext = facesContext.getExternalContext();
		Principal userPrincipal = externalContext.getUserPrincipal();
		if (userPrincipal == null) {
			return "";
		}

		return userPrincipal.getName();
	}

	public List<TipoFuncionario> getTiposFuncionarios() {
		List<TipoFuncionario> tipos = new ArrayList<TipoFuncionario>();
		tipos.add(TipoFuncionario.GARCOM);tipos.add(TipoFuncionario.COZINHEIRO);
		tipos.add(TipoFuncionario.CAIXA);		
		/*return TipoFuncionario.values();*/
		return tipos;
	}

	public FormaPagamento[] getTiposPagamento() {
		return FormaPagamento.values();
	}	

	public Plano[] getPlanos() {
		return Plano.values();
	}

	public Boolean[] getBooleans() {
		return new Boolean[] {true,false};
	}
	
	public String getMesEAnoAtual() {		
		return getMesAtual() + " de " + getAnoAtual();
	}

	private Integer getAnoAtual() {		
		return new Date(System.currentTimeMillis()).getYear()+1900;
	}

	private String getMesAtual() {
		String[] meses = new String[] {"Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"};
		return meses[new Date(System.currentTimeMillis()).getMonth()];
	}
}
