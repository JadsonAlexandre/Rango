package br.com.rango.beans;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import br.com.rango.entities.Funcionario;
import br.com.rango.entities.TipoFuncionario;
import br.com.rango.entities.TopGarcom;
import br.com.rango.entities.TopProduto;
import br.com.rango.service.FuncionarioService;
import br.com.rango.service.ItemPedidoService;

@Named(value="dashboardBean")
@ViewScoped
public class DashboardBean extends AbstractBean {

	private static final long serialVersionUID = 1L;	
	private String pagina ="/WEB-INF/dash_inicial.xhtml";
	private String classeBotaoInicio = "active";
	private String classeBotaoFuncionarios = "";
	private String classeBotaoMesas = "";
	private String classeBotaoEstoque = "";
	private String classeBotaoPedidos = "";	
	private String classeBotaoEstabelecimento = "";	

	@Inject
	FuncionarioService funcionarioService;
	
	@Inject
	ItemPedidoService itemPedidoService;

	private Funcionario usuario;
	private List<TopGarcom> topGarcons;
	private List<TopProduto> topProdutos;

	public void clicarInicio() {
		pagina = "/WEB-INF/dash_inicial.xhtml";
		classeBotaoInicio = "active"; classeBotaoFuncionarios = ""; classeBotaoMesas = "";
		classeBotaoEstoque = ""; classeBotaoPedidos = ""; classeBotaoEstabelecimento = "";
	}
	public void clicarFuncionarios() {
		pagina = "/WEB-INF/funcionarios/dash_funcionarios.xhtml";
		classeBotaoInicio = ""; classeBotaoFuncionarios = "active"; classeBotaoMesas = "";
		classeBotaoEstoque = ""; classeBotaoPedidos = ""; classeBotaoEstabelecimento = "";
	}
	public void clicarMesas() {
		pagina = "/WEB-INF/mesas/dash_mesas.xhtml";
		classeBotaoInicio = ""; classeBotaoFuncionarios = ""; classeBotaoMesas = "active";
		classeBotaoEstoque = ""; classeBotaoPedidos = ""; classeBotaoEstabelecimento = "";
	}
	public void clicarEstoque() {
		pagina = "/WEB-INF/estoque/dash_estoque.xhtml";
		classeBotaoInicio = ""; classeBotaoFuncionarios = ""; classeBotaoMesas = "";
		classeBotaoEstoque = "active"; classeBotaoPedidos = ""; classeBotaoEstabelecimento = "";
	}
	public void clicarPedidos() {
		pagina = "/WEB-INF/pedidos/dash_pedidos.xhtml";
		classeBotaoInicio = ""; classeBotaoFuncionarios = ""; classeBotaoMesas = "";
		classeBotaoEstoque = ""; classeBotaoPedidos = "active"; classeBotaoEstabelecimento = "";
	}
	public void clicarEstabelecimento() {
		pagina = "/WEB-INF/estabelecimento/dash_estabelecimento.xhtml";
		classeBotaoInicio = ""; classeBotaoFuncionarios = ""; classeBotaoMesas = "";
		classeBotaoEstoque = ""; classeBotaoPedidos = ""; classeBotaoEstabelecimento = "active";
	}
	public void clicarPerfil() {
		pagina = "/WEB-INF/perfil.xhtml";
		classeBotaoInicio = ""; classeBotaoFuncionarios = ""; classeBotaoMesas = "";
		classeBotaoEstoque = ""; classeBotaoPedidos = ""; classeBotaoEstabelecimento = "";
	}


	public String init() throws IOException {			
		try {
			usuario = funcionarioService.buscarFuncionarioLogin(getUserLogin());
			if(usuario.getEstabelecimento()==null) {				
				return "/paginas/cadastro.xhtml?faces-redirect=true funcionario="+usuario.getLogin();				
			}
			else {
				//colocando usuario e estabelecimento no sessão	
				FacesContext fc = FacesContext.getCurrentInstance();
				fc.getExternalContext().getSessionMap().put("estabelecimento",usuario.getEstabelecimento());
				fc.getExternalContext().getSessionMap().put("usuario",usuario);
				
				topProdutos = itemPedidoService.buscarTopProdutos(usuario.getEstabelecimento(), new Date(System.currentTimeMillis()));
				if(isGerente().equals("true")) {
					topGarcons = funcionarioService.buscarTopGarcons(usuario.getEstabelecimento(), new Date(System.currentTimeMillis()));
					
				}
				else {
					topGarcons = new ArrayList<TopGarcom>();					
				}
				clicarInicio();	
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			notificarMensagemDeErro(e.getMessage());
		}		
		return null;
	}

	public String isGerente() {		
		if(usuario.getTipoFuncionario()==TipoFuncionario.GERENTE) {
			return "true";
		}
		return "false";
	}
	
	public boolean isCozinheiro() {		
		if(usuario.getTipoFuncionario()==TipoFuncionario.COZINHEIRO) {
			return true;
		}		
		return false;
	}
	
	public boolean isCaixa() {		
		if(usuario.getTipoFuncionario()==TipoFuncionario.CAIXA) {
			return true;
		}
		return false;
	}
	
	public String isGerenteOrCozinheiro() {
		if(usuario.getTipoFuncionario()==TipoFuncionario.GERENTE || usuario.getTipoFuncionario()==TipoFuncionario.COZINHEIRO) {
			return "true";
		}
		return "false";
	}
	
	public void atualizarUsuario() {
		try {
			usuario = funcionarioService.buscarFuncionarioLogin(getUserLogin());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}			
	}


	public Funcionario getUsuario() {
		return usuario;
	}
	public void setUsuario(Funcionario usuario) {
		this.usuario = usuario;
	}	
	public List<TopGarcom> getTopGarcons() {
		return topGarcons;
	}
	public void setTopGarcons(List<TopGarcom> topGarcons) {
		this.topGarcons = topGarcons;
	}	
	public List<TopProduto> getTopProdutos() {
		return topProdutos;
	}
	public void setTopProdutos(List<TopProduto> topProdutos) {
		this.topProdutos = topProdutos;
	}
	public String getPagina() {
		return pagina;
	}
	public void setPagina(String pagina) {
		this.pagina = pagina;
	}
	public String getClasseBotaoInicio() {
		return classeBotaoInicio;
	}
	public void setClasseBotaoInicio(String classeBotaoInicio) {
		this.classeBotaoInicio = classeBotaoInicio;
	}
	public String getClasseBotaoFuncionarios() {
		return classeBotaoFuncionarios;
	}
	public void setClasseBotaoFuncionarios(String classeBotaoFuncionarios) {
		this.classeBotaoFuncionarios = classeBotaoFuncionarios;
	}
	public String getClasseBotaoMesas() {
		return classeBotaoMesas;
	}	
	public void setClasseBotaoMesas(String classeBotaoMesas) {
		this.classeBotaoMesas = classeBotaoMesas;
	}
	public String getClasseBotaoEstoque() {
		return classeBotaoEstoque;
	}
	public void setClasseBotaoEstoque(String classeBotaoEstoque) {
		this.classeBotaoEstoque = classeBotaoEstoque;
	}
	public String getClasseBotaoPedidos() {
		return classeBotaoPedidos;
	}
	public void setClasseBotaoPedidos(String classeBotaoPedidos) {
		this.classeBotaoPedidos = classeBotaoPedidos;
	}
	public String getClasseBotaoEstabelecimento() {
		return classeBotaoEstabelecimento;
	}
	public void setClasseBotaoEstabelecimento(String classeBotaoEstabelecimento) {
		this.classeBotaoEstabelecimento = classeBotaoEstabelecimento;
	}	

}
