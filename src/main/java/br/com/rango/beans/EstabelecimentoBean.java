package br.com.rango.beans;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import br.com.rango.entities.Estabelecimento;
import br.com.rango.service.EstabelecimentoService;

@Named(value="estabelecimentoBean")
@ViewScoped
public class EstabelecimentoBean extends AbstractBean {

	private static final long serialVersionUID = -2195649539053974677L;
	
	@Inject
	private EstabelecimentoService service;
	
	private Estabelecimento estabelecimento;	
	
	@PostConstruct
	public void init() {
		FacesContext fc = FacesContext.getCurrentInstance();		
		estabelecimento = (Estabelecimento) fc.getExternalContext().getSessionMap().get("estabelecimento");			
	}
	
	public void atualizar() {
		try {
			String mensagem = service.atualizarEstabelecimeto(estabelecimento);					
			notificarMensagemDeSucesso(mensagem);
			esconderModal("modalEditarNomeEstabelecimento");
			esconderModal("modalEditarTelefoneEstabelecimento");
		} catch (Exception e) {
			notificarMensagemDeErro(e.getMessage());
		}		
	}
	
	public void switchDelivery() {
		try {
			estabelecimento.setAberto(!estabelecimento.isAberto());
			String mensagem = service.atualizarEstabelecimeto(estabelecimento);					
			notificarMensagemDeSucesso(mensagem);
		} catch (Exception e) {
			notificarMensagemDeErro(e.getMessage());
		}			
	}

	public Estabelecimento getEstabelecimento() {
		return estabelecimento;
	}
	public void setEstabelecimento(Estabelecimento estabelecimento) {
		this.estabelecimento = estabelecimento.clone();
	}	
}
