package br.com.rango.beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import br.com.rango.entities.Estabelecimento;
import br.com.rango.entities.Funcionario;
import br.com.rango.entities.ItemPedido;
import br.com.rango.entities.Mesa;
import br.com.rango.entities.Pedido;
import br.com.rango.entities.StatusItem;
import br.com.rango.service.ItemPedidoService;
import br.com.rango.service.MesaService;
import br.com.rango.service.PedidoService;

@Named(value="pedidoBean")
@ViewScoped
public class PedidoBean extends AbstractBean {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private MesaService mesaService;
	
	@Inject
	private PedidoService pedidoService;
	
	@Inject
	private ItemPedidoService itemPedidoService;
	
	private Pedido pedido;	
	private Pedido pedidoSelecionado;
	private ItemPedido item;
	private ItemPedido itemSelecionado;

	private List<Mesa> mesas;	
	private List<Pedido> pedidosAbertos;
	private List<Pedido> pedidosFechados;
	private List<ItemPedido> itensCozinha;

	private Estabelecimento estabelecimento;
	private Funcionario garcom;

	@PostConstruct
	public void init() {		
		//PEGANDO ESTABELECIMENTO	
		FacesContext fc = FacesContext.getCurrentInstance();		
		estabelecimento = (Estabelecimento) fc.getExternalContext().getSessionMap().get("estabelecimento");
		garcom = (Funcionario) fc.getExternalContext().getSessionMap().get("usuario");
		limpar();		
	}	

	private void limpar() {
		try {
			pedido = new Pedido();		
			item = new ItemPedido();
			itemSelecionado = new ItemPedido();
			mesas = (List<Mesa>) mesaService.buscarTodasMesas(estabelecimento);
			pedidosAbertos = (List<Pedido>) pedidoService.buscarTodosPedidosAbertos(estabelecimento);
			pedidosFechados = (List<Pedido>) pedidoService.buscarTodosPedidosFechados(estabelecimento);
			itensCozinha = buscarItensCozinha();
		} catch (Exception e) {
			notificarMensagemDeErro(e.getMessage());
		}			
	}

	public void cadastrar() {
		try {
			if(pedido.getTaxa()==null) {
				pedido.setTaxa(new Float(0));
			}
			pedido.setGarcom(garcom);
			pedido.setEstabelecimento(estabelecimento);
			pedido.setDataAbertura(new Date(System.currentTimeMillis()));
			pedido.setValorDesconto(new Float(0));
			pedido.setValorPago(new Float(0));
			pedido.setValorProdutos(new Float(0));
			String mensagem = pedidoService.cadastrarPedido(pedido);			
			notificarMensagemDeSucesso(mensagem);
			esconderModal("modalCriarPedido");
			limpar();
		} catch (Exception e) {
			reportarMensagemDeErro(e.getMessage());
		}		
	}
	
	public void fecharPedido() {
		try {
			pedidoSelecionado.setItens(itemPedidoService.buscarTodosDoPedido(pedidoSelecionado));
			if(!(pedidoSelecionado.getValorDesconto()>(pedidoSelecionado.subTotal()+pedidoSelecionado.getTaxa()))) {
				pedidoSelecionado.setValorProdutos(pedidoSelecionado.subTotal());
				pedidoSelecionado.setValorPago((pedidoSelecionado.getValorProdutos()+pedidoSelecionado.getTaxa())-pedidoSelecionado.getValorDesconto());
				pedidoSelecionado.setDataFechamento(new Date(System.currentTimeMillis()));		
				pedidoService.atualizarPedido(pedidoSelecionado);
				pedidoSelecionado.getMesa().setOcupada(false);
				mesaService.atualizarMesa(pedidoSelecionado.getMesa());
				notificarMensagemDeSucesso("Pedido finalizado com sucesso!");
				limpar();
				esconderModal("modalFecharPedido");
				esconderModal("modalOpcoesPedido");
			}
			else {
				notificarMensagemDeErro("Você não pode oferecer um valor de desconto maior que o preço do pedido!");				
			}
			
		} catch (Exception e) {
			notificarMensagemDeErro(e.getMessage());
		}
	}
	
	public void cancelarPedido() {
		try {			
			pedidoSelecionado.getMesa().setOcupada(false);
			mesaService.atualizarMesa(pedidoSelecionado.getMesa());
			pedidoService.removerPedido(pedidoSelecionado);
			notificarMensagemDeSucesso("Pedido cancelado com sucesso!");
			limpar();
			esconderModal("modalCancelarPedido");
			esconderModal("modalOpcoesPedido");						
		} catch (Exception e) {
			notificarMensagemDeErro(e.getMessage());
		}
	}

	public void adicionarItem() {		
		try {			
			if(item.getProduto().isDaCasa()) {
				item.setStatus(StatusItem.NA_FILA);
			}
			else {
				item.setStatus(StatusItem.PRONTO);
			}
			item.setPedido(pedidoSelecionado);	
			item.setEstabelecimento(estabelecimento);
			String mensagem = itemPedidoService.adicionarItem(item);			
			pedidoSelecionado.setItens(itemPedidoService.buscarTodosDoPedido(pedidoSelecionado));
			notificarMensagemDeSucesso(mensagem);
			esconderModal("modalQuantidadeItem");
			esconderModal("modalAdicionarItem");					
			limpar();	
		} catch (Exception e) {			
			notificarMensagemDeErro(e.getMessage());			
		}
	}

	public void editarItem() {
		try {			
			String mensagem = itemPedidoService.atualizarItem(itemSelecionado);
			notificarMensagemDeSucesso(mensagem);
			esconderModal("modalEditarItem");
			pedidoSelecionado.setItens(itemPedidoService.buscarTodosDoPedido(pedidoSelecionado));
			limpar();
		} catch (Exception e) {			
			notificarMensagemDeErro(e.getMessage());
		}
	}

	public void excluirItem() {
		try {
			String mensagem = itemPedidoService.removerItem(itemSelecionado);
			notificarMensagemDeSucesso(mensagem);
			esconderModal("modalExcluirItem");	
			pedidoSelecionado.setItens(itemPedidoService.buscarTodosDoPedido(pedidoSelecionado));
			limpar();
		} catch (Exception e) {
			notificarMensagemDeErro(e.getMessage());
		}
	}
	
	public void entregarItem() {
		try {
			itemSelecionado.setEntregue(true);
			String mensagem = itemPedidoService.atualizarItem(itemSelecionado);
			notificarMensagemDeSucesso(mensagem);
			esconderModal("modalEntregarItem");	
			pedidoSelecionado.setItens(itemPedidoService.buscarTodosDoPedido(pedidoSelecionado));
			limpar();			
		} catch (Exception e) {
			notificarMensagemDeErro(e.getMessage());
		}		
	}
	
	public void prepararItem() {
		try {
			itemSelecionado.setStatus(StatusItem.EM_PREPARO);
			itemPedidoService.atualizarItem(itemSelecionado);
			esconderModal("modalPrepararItem");
			notificarMensagemDeSucesso("Item em Preparo!");			
		} catch (Exception e) {
			notificarMensagemDeErro(e.getMessage());
		}
	}
	
	public void itemPronto() {
		try {
			itemSelecionado.setStatus(StatusItem.PRONTO);
			itemPedidoService.atualizarItem(itemSelecionado);
			esconderModal("modalItemPronto");
			notificarMensagemDeSucesso("Item pronto e à espera de entrega!");			
		} catch (Exception e) {
			notificarMensagemDeErro(e.getMessage());
		}
	}
	
	public int pedidosDoMes() {
		try {
			return pedidoService.buscarTodosPedidosMes(estabelecimento).size();
		} 
		catch (Exception e) {
			notificarMensagemDeErro(e.getMessage());
		}
		return 0;
	}
	
	public float caixaDoMes() {
		try {
			List<Pedido> pedidos = (List<Pedido>) pedidoService.buscarTodosPedidosMes(estabelecimento);
			float caixa = 0;
			for(Pedido p : pedidos) {
				caixa += p.getValorPago();
			}
			return caixa;
		} 
		catch (Exception e) {
			notificarMensagemDeErro(e.getMessage());
		}
		return 0;
	}
	
	public List<ItemPedido> buscarItensCozinha() {
		try {
			return itemPedidoService.buscarTodosCozinha(estabelecimento);
		} 
		catch (Exception e) {
			notificarMensagemDeErro(e.getMessage());
		}
		return new ArrayList<ItemPedido>();		
	}
	
	public void atualizarItensCozinha() {
		itensCozinha = buscarItensCozinha();		
	}
	
	public void atualizarPedidosAbertos() {
		try {
			pedidosAbertos = (List<Pedido>) pedidoService.buscarTodosPedidosAbertos(estabelecimento);
			if(pedidoSelecionado!=null) {
				pedidoSelecionado.setItens(itemPedidoService.buscarTodosDoPedido(pedidoSelecionado));
			}
		} catch (Exception e) {			
			notificarMensagemDeErro(e.getMessage());
		}
	}

	//GETTERS AND SETTER	
	public List<Mesa> getMesas() {
		return mesas;
	}
	public Pedido getPedido() {
		return pedido;
	}
	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}
	public Estabelecimento getEstabelecimento() {
		return estabelecimento;
	}
	public void setEstabelecimento(Estabelecimento estabelecimento) {
		this.estabelecimento = estabelecimento;
	}
	public void setMesas(List<Mesa> mesas) {
		this.mesas = mesas;
	}		
	public List<Pedido> getPedidosAbertos() {
		return pedidosAbertos;
	}
	public void setPedidosAbertos(List<Pedido> pedidosAbertos) {
		this.pedidosAbertos = pedidosAbertos;
	}
	public List<Pedido> getPedidosFechados() {
		return pedidosFechados;
	}
	public void setPedidosFechados(List<Pedido> pedidosFechados) {
		this.pedidosFechados = pedidosFechados;
	}	
	public ItemPedido getItem() {
		return item;
	}
	public void setItem(ItemPedido item) {
		this.item = item;
	}
	public ItemPedido getItemSelecionado() {
		return itemSelecionado;
	}
	public void setItemSelecionado(ItemPedido itemSelecionado) {
		this.itemSelecionado = itemSelecionado;
	}
	public Pedido getPedidoSelecionado() {
		return pedidoSelecionado;
	}
	public void setPedidoSelecionado(Pedido pedidoSelecionado) {
		this.pedidoSelecionado = pedidoSelecionado;
	}
	public List<ItemPedido> getItensCozinha() {
		return itensCozinha;
	}
	public void setItensCozinha(List<ItemPedido> itensCozinha) {
		this.itensCozinha = itensCozinha;
	}
	
}
