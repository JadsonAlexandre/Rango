package br.com.rango.beans;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import br.com.rango.entities.Estabelecimento;
import br.com.rango.entities.Funcionario;
import br.com.rango.service.FuncionarioService;

@Named(value="funcionarioBean")
@ViewScoped
public class FuncionarioBean extends AbstractBean {

	private static final long serialVersionUID = 3404559077519407308L;

	@Inject
	private FuncionarioService service;

	private Funcionario funcionario;
	private Funcionario funcionarioSelecionado;
	private List<Funcionario> funcionarios;
	private Estabelecimento estabelecimento;	
	private String confirmarSenha;	

	@PostConstruct
	public void init(){
		//PEGANDO ESTABELECIMENTO	
		FacesContext fc = FacesContext.getCurrentInstance();		
		estabelecimento = (Estabelecimento) fc.getExternalContext().getSessionMap().get("estabelecimento");	
		limpar();		
	}

	public void limpar() {		
		FacesContext fc = FacesContext.getCurrentInstance();		
		funcionario = new Funcionario();
		try {			
			funcionarios = service.buscarTodosFuncionarios(estabelecimento);
			filtrarFuncionarios((Funcionario) fc.getExternalContext().getSessionMap().get("usuario"));
		} catch (Exception e) {
			notificarMensagemDeErro(e.getMessage());
		}
	}

	public void cadastrar() {
		if(funcionario.getSenha().equals(confirmarSenha)) {
			try {				
				funcionario.setEstabelecimento(estabelecimento);
				String mensagem = service.cadastrarFuncionario(funcionario);
				notificarMensagemDeSucesso(mensagem);
				limpar();
			} catch (Exception e) {
				reportarMensagemDeErro(e.getMessage());
			}
		}
		else {
			reportarMensagemDeErro("As senhas não conferem! Por favor, reinsira a senha");
		}
	}

	public void atualizar() {
		try {
			String mensagem = service.atualizarFuncionario(funcionarioSelecionado);
			notificarMensagemDeSucesso(mensagem);
			esconderModal("modalEditarFuncionario");
			limpar();
		} catch (Exception e) {
			reportarMensagemDeErro(e.getMessage());
		}		
	}

	public void excluir() {
		try {
			String mensagem = service.removerFuncionario(funcionarioSelecionado);
			notificarMensagemDeSucesso(mensagem);
			limpar();
		} catch (Exception e) {
			notificarMensagemDeErro(e.getMessage());
		}			
	}
	
	public void checkarLogin() {
		try {
			if(service.buscarFuncionarioLogin(funcionario.getLogin())==null) {
				notificarMensagemDeSucesso("Login disponível!");
			}
			else {
				notificarMensagemDeErro("Login indisponível!");				
			}
		} catch (Exception e) {
			reportarMensagemDeErro(e.getMessage());
		}
	}
	
	public void checkarLoginEditado() {
		try {
			if(service.validarAtualizacao(funcionarioSelecionado)) {
				notificarMensagemDeSucesso("Login disponível!");
			}
			else {
				notificarMensagemDeErro("Login indisponível!");				
			}
		} catch (Exception e) {
			reportarMensagemDeErro(e.getMessage());
		}
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}
	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}
	public List<Funcionario> getFuncionarios() {
		return funcionarios;
	}
	public void setFuncionarios(List<Funcionario> funcionarios) {
		this.funcionarios = funcionarios;
	}
	public String getConfirmarSenha() {
		return confirmarSenha;
	}
	public void setConfirmarSenha(String confirmarSenha) {
		this.confirmarSenha = confirmarSenha;
	}
	public Estabelecimento getEstabelecimento() {
		return estabelecimento;
	}
	public void setEstabelecimento(Estabelecimento estabelecimento) {
		this.estabelecimento = estabelecimento;
	}

	public Funcionario getFuncionarioSelecionado() {
		return funcionarioSelecionado;
	}
	public void setFuncionarioSelecionado(Funcionario funcionarioSelecionado) {
		this.funcionarioSelecionado = funcionarioSelecionado.clone();
	}
	
	public void filtrarFuncionarios(Funcionario funcionario) {
		int i = -1;
		for(Funcionario f: funcionarios) {
			if(f.getIdFuncionario()==funcionario.getIdFuncionario()) {
				i = funcionarios.indexOf(f);
			}
		}
		if(i>=0) {
			funcionarios.remove(i);
		}		
	}
	public int contFuncionarios() {
		return funcionarios.size();
	}
}
