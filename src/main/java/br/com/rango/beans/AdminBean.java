package br.com.rango.beans;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import br.com.rango.entities.Cadastro;
import br.com.rango.entities.Estabelecimento;
import br.com.rango.service.CadastroService;
import br.com.rango.service.EstabelecimentoService;

@Named(value="adminBean")
@RequestScoped
public class AdminBean extends AbstractBean {

	private static final long serialVersionUID = 6297949126217332867L;
	
	@Inject
	private CadastroService cadastroService;
	
	@Inject
	private EstabelecimentoService estabelecimentoService;
	
	private List<Cadastro> cadastros;
	private List<Estabelecimento> estabelecimentos;
	
	private Cadastro cadastroSelecionado;
	
	@PostConstruct
	public void init() {
		try {
			cadastros = cadastroService.buscarTodosCadastros();
			estabelecimentos = estabelecimentoService.buscarTodosEstabelecimentos();
			cadastroSelecionado = new Cadastro();					
		} catch (Exception e) {
			reportarMensagemDeErro(e.getMessage());
		}		
	}
	
	public void confirmarPagamentoCadastro() {
		if(cadastroSelecionado.getIdCadastro()>0) {
			try {
				cadastroService.confirmarPagamentoCadastro(cadastroSelecionado);				
			} catch (Exception e) {
				reportarMensagemDeErro(e.getMessage());
			}
			init();
		}		
	}
	
	public void removerCadastro() {
		if(cadastroSelecionado.getIdCadastro()>0) {
			try {
				cadastroService.removerCadastro(cadastroSelecionado);				
			} catch (Exception e) {
				reportarMensagemDeErro(e.getMessage());
			}
			init();
		}			
	}

	/*GET AND SETTERS*/
	public List<Cadastro> getCadastros() {
		return cadastros;
	}
	public void setCadastros(List<Cadastro> cadastros) {
		this.cadastros = cadastros;
	}
	public List<Estabelecimento> getEstabelecimentos() {
		return estabelecimentos;
	}
	public void setEstabelecimentos(List<Estabelecimento> estabelecimentos) {
		this.estabelecimentos = estabelecimentos;
	}
	public Cadastro getCadastroSelecionado() {
		return cadastroSelecionado;
	}
	public void setCadastroSelecionado(Cadastro cadastroSelecionado) {
		this.cadastroSelecionado = cadastroSelecionado;
	}	
}
