package br.com.rango.beans;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import br.com.rango.entities.Funcionario;
import br.com.rango.entities.TipoFuncionario;
import br.com.rango.service.FuncionarioService;

@Named(value="perfilBean")
@ViewScoped
public class PerfilBean extends AbstractBean {

	private static final long serialVersionUID = 1504963037907584752L;
	
	@Inject
	private FuncionarioService funcionarioService;
	
	private Funcionario usuario;
	
	private String antigaSenha;
	
	private String novaSenha;
	
	private String confirmarNovaSenha;
	
	@PostConstruct
	public void init() {
		try {
			usuario = funcionarioService.buscarFuncionarioLogin(getUserLogin());
			limpar();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	public void limpar() {
		antigaSenha = "";
		novaSenha = "";
		confirmarNovaSenha = "";
	}
	
	public void atualizarUsuario() {
		try {
			String mensagem = funcionarioService.atualizarFuncionario(usuario);			
			notificarMensagemDeSucesso(mensagem);			
			init();
		} catch (Exception e) {
			reportarMensagemDeErro(e.getMessage());
		}
	}
	
	public void atualizarSenha() {
		try {
			if(funcionarioService.hash(antigaSenha).equals(usuario.getSenha())){
				if(novaSenha.equals(confirmarNovaSenha)) {
					usuario.setSenha(novaSenha);
					String mensagem = funcionarioService.atualizarSenhaFuncionario(usuario);
					limpar();
					notificarMensagemDeSucesso(mensagem);	
					esconderModal("modalEditarSenha");
				}
				else {
					reportarMensagemDeErro("A nova senha não confere!");
				}								
			}
			else {
				reportarMensagemDeErro("A senha atual não confere!");
			}
		} catch (Exception e) {
			notificarMensagemDeErro(e.getMessage());
		}
	}
	
	public void checkarLogin() {
		try {
			if(funcionarioService.validarAtualizacao(usuario)) {
				notificarMensagemDeSucesso("Login disponível!");
			}
			else {
				notificarMensagemDeErro("Login indisponível!");				
			}
		} catch (Exception e) {
			reportarMensagemDeErro(e.getMessage());
		}
	}
	
	//getter and setters
	public Funcionario getUsuario() {
		return usuario;
	}
	public void setUsuario(Funcionario usuario) {
		this.usuario = usuario;
	}	
	public String getAntigaSenha() {
		return antigaSenha;
	}
	public void setAntigaSenha(String antigaSenha) {
		this.antigaSenha = antigaSenha;
	}

	public String getNovaSenha() {
		return novaSenha;
	}
	public void setNovaSenha(String novaSenha) {
		this.novaSenha = novaSenha;
	}
	public String getConfirmarNovaSenha() {
		return confirmarNovaSenha;
	}
	public void setConfirmarNovaSenha(String confirmarNovaSenha) {
		this.confirmarNovaSenha = confirmarNovaSenha;
	}
	public boolean isGerente() {
		if(usuario.getTipoFuncionario()==TipoFuncionario.GERENTE) {
			return true;
		}
		return false;
	}
	

}
