package br.com.rango.beans;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import br.com.rango.entities.Estabelecimento;
import br.com.rango.entities.Mesa;
import br.com.rango.service.MesaService;

@Named(value="mesaBean")
@ViewScoped
public class MesaBean extends AbstractBean {

	private static final long serialVersionUID = 4371924225470682217L;
	
	@Inject
	private MesaService service;
	
	private Mesa mesa;
	private Mesa mesaSelecionada;
	private List<Mesa> mesas;
	private Estabelecimento estabelecimento;
	
	@PostConstruct
	public void init() {		
		//PEGANDO ESTABELECIMENTO	
		FacesContext fc = FacesContext.getCurrentInstance();		
		estabelecimento = (Estabelecimento) fc.getExternalContext().getSessionMap().get("estabelecimento");	
		limpar();		
	}
	
	public void limpar() {
		mesa = new Mesa();
		try {
			mesas = (List<Mesa>) service.buscarTodasMesas(estabelecimento);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			reportarMensagemDeErro(e.getMessage());
		}
	}
	
	public void cadastrar() {		
		try {					
			mesa.setEstabelecimento(estabelecimento);
			String mensagem = service.cadastrarMesa(mesa);
			notificarMensagemDeSucesso(mensagem);
			limpar();
		} catch (Exception e) {
			reportarMensagemDeErro(e.getMessage());
		}
	}
	
	public void atualizar() {
		try {
			String mensagem = service.atualizarMesa(mesaSelecionada);
			notificarMensagemDeSucesso(mensagem);
			esconderModal("modalEditarMesa");			
			limpar();
		} catch (Exception e) {
			reportarMensagemDeErro(e.getMessage());
		}		
	}
	
	public void excluir() {
		try {
			String mensagem = service.removerMesa(mesaSelecionada);
			notificarMensagemDeSucesso(mensagem);			
			limpar();
		} catch (Exception e) {
			notificarMensagemDeErro(e.getMessage());
		}			
	}
	
	public String contMesas() {
		int mesasOcupadas = 0;
		if(mesas.size()>0) {			
			for(Mesa m: mesas) {
				if(m.isOcupada()) {
					mesasOcupadas++;
				}
			}	

		}
		return mesasOcupadas+"/"+mesas.size();	
	}

	public Mesa getMesa() {
		return mesa;
	}
	public void setMesa(Mesa mesa) {
		this.mesa = mesa;
	}
	public List<Mesa> getMesas() {
		return mesas;
	}
	public void setMesas(List<Mesa> mesas) {
		this.mesas = mesas;
	}
	public Mesa getMesaSelecionada() {
		return mesaSelecionada;
	}
	public void setMesaSelecionada(Mesa mesaSelecionada) {
		this.mesaSelecionada = mesaSelecionada.clone();
	}
	public Estabelecimento getEstabelecimento() {
		return estabelecimento;
	}
	public void setEstabelecimento(Estabelecimento estabelecimento) {
		this.estabelecimento = estabelecimento;
	}
	

}
