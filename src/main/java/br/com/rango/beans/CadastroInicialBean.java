package br.com.rango.beans;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import br.com.rango.entities.Estabelecimento;
import br.com.rango.entities.Funcionario;
import br.com.rango.service.FuncionarioService;

@Named(value="cadastroInicialBean")
@ViewScoped
public class CadastroInicialBean extends AbstractBean {

	private static final long serialVersionUID = 8825287261068413546L;

	private Estabelecimento estabelecimento;
	private Funcionario funcionario;	
	private String senha;	

	@Inject
	private FuncionarioService funcionarioService;	

	public String init() {
		try {
			funcionario = funcionarioService.buscarFuncionarioLogin(getUserLogin());
			if(funcionario.getEstabelecimento()!=null) {				
				return "/dashboard";				
			}
			else {
				estabelecimento = new Estabelecimento();
				funcionario.setLogin("");
				funcionario.setSenha("");				
			}
		} catch (Exception e) {
			notificarMensagemDeErro(e.getMessage());
			reportarMensagemDeErro(e.getMessage());
		}
		return null;
	}

	public String cadastrar() {	
		if(funcionario.getSenha().equals(senha)) {
			try {					
				estabelecimento.setGerente(funcionario);				
				funcionario.setEstabelecimento(estabelecimento);				
				funcionarioService.cadastroInicial(funcionario, estabelecimento);
				return "/paginas/tudo_pronto.xhtml";			
			} catch (Exception e) {
				reportarMensagemDeErro(e.getMessage());
			}
			return null;			
		}
		else {
			reportarMensagemDeErro("As senhas não são iguais. Por favor, reinsira sua sneha");
			return null;
		}				
	}

	/*GETTERS AND SETTER */
	public Estabelecimento getEstabelecimento() {
		return estabelecimento;
	}
	public void setEstabelecimento(Estabelecimento estabelecimento) {
		this.estabelecimento = estabelecimento;
	}
	public Funcionario getFuncionario() {
		return funcionario;
	}
	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}	
}
