package br.com.rango.beans;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import br.com.rango.entities.Cadastro;
import br.com.rango.service.CadastroService;

@Named(value="register")
@ViewScoped
public class RegisterBean extends AbstractBean {

	private static final long serialVersionUID = 1L;
	
	private Cadastro cadastro;
	
	@Inject
	private CadastroService service;
	
	@PostConstruct
	public void init(){
		if(cadastro == null){
			cadastro = new Cadastro();			
		}		
	}
	
	public String submit(){
		try {
			return service.cadastrarGerente(cadastro);
		} catch (Exception e) {
			reportarMensagemDeErro(e.getMessage());
			return null;
		}		
	}	

	public Cadastro getCadastro() {
		return cadastro;
	}

	public void setGerente(Cadastro cadastro) {
		this.cadastro = cadastro;
	}	
}
