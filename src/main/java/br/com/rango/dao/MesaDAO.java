package br.com.rango.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import br.com.rango.entities.Estabelecimento;
import br.com.rango.entities.Mesa;

public class MesaDAO extends DAO {

	private static final long serialVersionUID = -1278629284697721448L;

	public void save(Mesa obj) throws Exception {
		EntityManager em = getEntityManager();
		try {
			em.persist(obj);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new Exception("Ocorreu algum erro ao tentar salvar a mesa.", pe);
		}
	}

	public Mesa update(Mesa obj) throws Exception {
		EntityManager em = getEntityManager();
		Mesa resultado = obj;
		try {
			resultado = em.merge(obj);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new Exception("Ocorreu algum erro ao tentar atualizar a mesa.", pe);
		}
		return resultado;
	}

	public void delete(Mesa obj) throws Exception {
		EntityManager em = getEntityManager();
		try {
			obj = em.find(Mesa.class, obj.getId());
			em.remove(obj);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new Exception("Ocorreu algum erro ao tentar remover a mesa.", pe);
		}
	}

	public Mesa getByID(Long objId) throws Exception {
		EntityManager em = getEntityManager();
		Mesa resultado = null;
		try {
			resultado = em.find(Mesa.class, objId);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new Exception("Ocorreu algum erro ao tentar recuperar a mesa com base no ID.", pe);
		}

		return resultado;
	}

	public List<Mesa> getAll(Estabelecimento e) {
		EntityManager em = getEntityManager();
		//JPQL
		String jpql = "SELECT mesa FROM Mesa mesa INNER JOIN mesa.estabelecimento e"
				+ " WHERE (e = :estabelecimento) ORDER BY mesa.numero";		

		//CRIANDO QUERY
		TypedQuery<Mesa> consulta = em.createQuery(jpql, Mesa.class).setParameter("estabelecimento", e);		

		//OBTENDO RESULTADOS
		return consulta.getResultList();		
	}

	public Mesa getByNumber(Mesa mesa) {
		EntityManager em = getEntityManager();
		Mesa mesaBusca = null;		
		try {
			//JPQL
			String jpql = "SELECT mesa FROM Mesa mesa INNER JOIN mesa.estabelecimento e"
					+ " WHERE (e = :estabelecimento AND mesa.numero = :numero)";		

			//CRIANDO QUERY
			mesaBusca = em.createQuery(jpql, Mesa.class).setParameter("estabelecimento", mesa.getEstabelecimento())
					.setParameter("numero", mesa.getNumero()).getSingleResult();	

		}catch (NoResultException e) {
			mesaBusca = null;
		}catch (RuntimeException e) {
			e.printStackTrace();
		}
		return mesaBusca;
	}
}
