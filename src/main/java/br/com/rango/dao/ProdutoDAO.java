package br.com.rango.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import br.com.rango.entities.Estabelecimento;
import br.com.rango.entities.Produto;
import br.com.rango.entities.TipoProduto;

public class ProdutoDAO extends DAO {

	private static final long serialVersionUID = 6007665415180323830L;

	public void save(Produto obj) throws Exception {
		EntityManager em = getEntityManager();
		try {
			em.persist(obj);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new Exception("Ocorreu algum erro ao tentar salvar o produto.", pe);
		}
	}

	public Produto update(Produto obj) throws Exception {
		EntityManager em = getEntityManager();
		Produto resultado = obj;
		try {
			resultado = em.merge(obj);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new Exception("Ocorreu algum erro ao tentar atualizar o produto.", pe);
		}
		return resultado;
	}

	public void delete(Produto obj) throws Exception {
		EntityManager em = getEntityManager();
		try {
			obj = em.find(Produto.class, obj.getId());
			em.remove(obj);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new Exception("Ocorreu algum erro ao tentar remover o produto.", pe);
		}
	}

	public Produto getByID(Long objId) throws Exception {
		EntityManager em = getEntityManager();
		Produto resultado = null;
		try {
			resultado = em.find(Produto.class, objId);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new Exception("Ocorreu algum erro ao tentar recuperar o produto com base no ID.", pe);
		}
		return resultado;
	}

	public List<Produto> getAll(Estabelecimento estabelecimento) {
		EntityManager em = getEntityManager();
		//JPQL
		String jpql = "SELECT produto FROM Produto produto INNER JOIN produto.estabelecimento e"
				+ " WHERE (e = :estabelecimento) ORDER BY produto.tipo.tipo";		

		//CRIANDO QUERY
		TypedQuery<Produto> consulta = em.createQuery(jpql, Produto.class).setParameter("estabelecimento", estabelecimento);		

		//OBTENDO RESULTADOS
		return consulta.getResultList();
	}

	public Produto getByName(Produto produto) {
		EntityManager em = getEntityManager();
		Produto produtoBusca = null;
		try {
			//JPQL
			String jpql = "SELECT produto FROM Produto produto INNER JOIN produto.estabelecimento e"
					+ " WHERE (e = :estabelecimento AND produto.nome = :nome)";		

			//CRIANDO QUERY
			produtoBusca = em.createQuery(jpql, Produto.class).setParameter("estabelecimento", produto.getEstabelecimento())
					.setParameter("nome", produto.getNome()).getSingleResult();	

		}catch (NoResultException e) {
			produtoBusca = null;
		}catch (RuntimeException e) {
			e.printStackTrace();
		}
		return produtoBusca;
	}

	public List<Produto> getAllByType(TipoProduto tipo) {
		EntityManager em = getEntityManager();	

		//JPQL
		String jpql = "SELECT produto FROM Produto produto INNER JOIN produto.tipo t"
				+ " WHERE (t = :tipo) ORDER BY produto.nome";		

		//CRIANDO QUERY
		TypedQuery<Produto> consulta = em.createQuery(jpql, Produto.class).setParameter("tipo", tipo);		

		//OBTENDO RESULTADOS
		return consulta.getResultList();

	}



}
