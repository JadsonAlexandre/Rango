package br.com.rango.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import br.com.rango.entities.Cadastro;

public class CadastroDAO extends DAO {

	private static final long serialVersionUID = 8123263960051649623L;

	public void save(Cadastro obj) throws Exception {
		EntityManager em = getEntityManager();
		try {
			em.persist(obj);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new Exception("Ocorreu um erro ao tentar salvar o cadastro.", pe);
		}
	}

	public Cadastro update(Cadastro obj) throws Exception {
		EntityManager em = getEntityManager();
		Cadastro resultado = obj;
		try {
			resultado = em.merge(obj);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new Exception("Ocorreu algum erro ao tentar atualizar o cadastro.", pe);
		}
		return resultado;
	}

	public void delete(Cadastro obj) throws Exception {
		EntityManager em = getEntityManager();
		try {
			obj = em.find(Cadastro.class, obj.getIdCadastro());
			em.remove(obj);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new Exception("Ocorreu algum erro ao tentar remover o cadastro.", pe);
		}
	}

	public Cadastro getByID(Long objId) throws Exception {
		EntityManager em = getEntityManager();
		Cadastro resultado = null;
		try {
			resultado = em.find(Cadastro.class, objId);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new Exception("Ocorreu algum erro ao tentar recuperar o cadastro com base no ID.", pe);
		}
		return resultado;
	}

	public List<Cadastro> getAll() throws Exception {
		EntityManager em = getEntityManager();
		
		//JPQL
		String jpql = "SELECT cadastro FROM Cadastro cadastro";		

		//CRIANDO QUERY
		TypedQuery<Cadastro> consulta = em.createQuery(jpql, Cadastro.class);

		//OBTENDO RESULTADOS
		return consulta.getResultList();
	}
}
