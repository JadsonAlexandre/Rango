package br.com.rango.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import br.com.rango.entities.Estabelecimento;
import br.com.rango.entities.TipoProduto;

public class TipoProdutoDAO extends DAO {

	private static final long serialVersionUID = -2782063745557717165L;

	public void save(TipoProduto obj) throws Exception {
		EntityManager em = getEntityManager();
		try {
			em.persist(obj);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new Exception("Ocorreu algum erro ao tentar salvar a categoria.", pe);
		}
	}

	public TipoProduto update(TipoProduto obj) throws Exception {
		EntityManager em = getEntityManager();
		TipoProduto resultado = obj;
		try {
			resultado = em.merge(obj);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new Exception("Ocorreu algum erro ao tentar atualizar a categoria.", pe);
		}
		return resultado;
	}

	public void delete(TipoProduto obj) throws Exception {
		EntityManager em = getEntityManager();
		try {
			obj = em.find(TipoProduto.class, obj.getId());
			em.remove(obj);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new Exception("Ocorreu algum erro ao tentar remover a categoria.", pe);
		}
	}

	public TipoProduto getByID(Long objId) throws Exception {
		EntityManager em = getEntityManager();
		TipoProduto resultado = null;
		try {
			resultado = em.find(TipoProduto.class, objId);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new Exception("Ocorreu algum erro ao tentar recuperar a categoria com base no ID.", pe);
		}
		return resultado;
	}

	public List<TipoProduto> getAll(Estabelecimento estabelecimento) {
		EntityManager em = getEntityManager();
		//JPQL
		String jpql = "SELECT tipoProduto FROM TipoProduto tipoProduto INNER JOIN tipoProduto.estabelecimento e"
				+ " WHERE (e = :estabelecimento)";		

		//CRIANDO QUERY
		TypedQuery<TipoProduto> consulta = em.createQuery(jpql, TipoProduto.class).setParameter("estabelecimento", estabelecimento);		

		//OBTENDO RESULTADOS
		return consulta.getResultList();
	}

	public TipoProduto getByName(TipoProduto tipo) {
		EntityManager em = getEntityManager();
		TipoProduto tipoBusca = null;

		try {
			//JPQL
			String jpql = "SELECT tipoProduto FROM TipoProduto tipoProduto INNER JOIN tipoProduto.estabelecimento e"
					+ " WHERE (e = :estabelecimento AND tipoProduto.tipo = :tipo)";		

			//CRIANDO QUERY
			tipoBusca = em.createQuery(jpql, TipoProduto.class).setParameter("estabelecimento", tipo.getEstabelecimento())
					.setParameter("tipo", tipo.getTipo()).getSingleResult();	

		}catch (NoResultException e) {
			tipoBusca = null;
		}catch (RuntimeException e) {
			e.printStackTrace();
		}
		return tipoBusca;
	}

}
