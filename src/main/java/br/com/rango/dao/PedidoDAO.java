package br.com.rango.dao;

import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import br.com.rango.entities.Estabelecimento;
import br.com.rango.entities.Funcionario;
import br.com.rango.entities.Pedido;

public class PedidoDAO extends DAO {

	private static final long serialVersionUID = 2785984314857455327L;
	
	public void save(Pedido obj) throws Exception {
		EntityManager em = getEntityManager();
		try {
			em.persist(obj);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new Exception("Ocorreu algum erro ao tentar salvar o pedido.", pe);
		}
	}
	
	public Pedido update(Pedido obj) throws Exception {
		EntityManager em = getEntityManager();
		Pedido resultado = obj;
		try {
			resultado = em.merge(obj);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new Exception("Ocorreu algum erro ao tentar atualizar o pedido.", pe);
		}
		return resultado;
	}
	
	public void delete(Pedido obj) throws Exception {
		EntityManager em = getEntityManager();
		try {
			obj = em.find(Pedido.class, obj.getIdPedido());
			em.remove(obj);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new Exception("Ocorreu algum erro ao tentar remover o pedido.", pe);
		}
	}
	
	public Pedido getByID(Long objId) throws Exception {
		EntityManager em = getEntityManager();
		Pedido resultado = null;
		try {
			resultado = em.find(Pedido.class, objId);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new Exception("Ocorreu algum erro ao tentar recuperar o usuário com base no ID.", pe);
		}
		return resultado;
	}
	
	public List<Pedido> getAllOpen(Estabelecimento e) {
		EntityManager em = getEntityManager();
		//JPQL
		String jpql = "SELECT pedido FROM Pedido pedido INNER JOIN pedido.estabelecimento e"
				+ " WHERE (e = :estabelecimento AND pedido.valorPago = 0) ORDER BY pedido.dataAbertura DESC";		

		//CRIANDO QUERY
		TypedQuery<Pedido> consulta = em.createQuery(jpql, Pedido.class).setParameter("estabelecimento", e);		

		//OBTENDO RESULTADOS
		return consulta.getResultList();	
	}
	
	public List<Pedido> getAllClosed(Estabelecimento e) {
		EntityManager em = getEntityManager();
		//JPQL
		String jpql = "SELECT pedido FROM Pedido pedido INNER JOIN pedido.estabelecimento e"
				+ " WHERE (e = :estabelecimento AND pedido.valorPago != 0) ORDER BY pedido.dataFechamento DESC";		

		//CRIANDO QUERY
		TypedQuery<Pedido> consulta = em.createQuery(jpql, Pedido.class).setParameter("estabelecimento", e);		

		//OBTENDO RESULTADOS
		return consulta.getResultList();	
	}
	
	public List<Pedido> getAllByMoth(Estabelecimento e, Date mes){
		EntityManager em = getEntityManager();
		//JPQL
		String jpql = "SELECT pedido FROM Pedido pedido INNER JOIN pedido.estabelecimento e"
				+ " WHERE (e = :estabelecimento AND pedido.valorPago != 0 AND month(pedido.dataAbertura) = :mes  AND year(pedido.dataAbertura) = :ano)";		
		
		//month(pedido.dataAbertura) = 12 -> Dezembro -> data.getMonth()+1
		//year(pedido.dataAbertura) = 2017 -> data.getYear()+1900
		//CRIANDO QUERY
		TypedQuery<Pedido> consulta = em.createQuery(jpql, Pedido.class).setParameter("estabelecimento", e).setParameter("mes", mes.getMonth()+1).setParameter("ano", mes.getYear()+1900);		

		//OBTENDO RESULTADOS
		return consulta.getResultList();		
	}

	public List<Pedido> getOrderByEmployeeInDate(Funcionario funcionario, Date data) {
		EntityManager em = getEntityManager();
		//JPQL
		String jpql = "SELECT pedido FROM Pedido pedido INNER JOIN pedido.garcom g"
				+ " WHERE (g = :garcom AND pedido.valorPago != 0 AND month(pedido.dataAbertura) = :mes AND year(pedido.dataAbertura) = :ano)";		
		
		
		//CRIANDO QUERY
		TypedQuery<Pedido> consulta = em.createQuery(jpql, Pedido.class).setParameter("garcom", funcionario).setParameter("mes", data.getMonth()+1).setParameter("ano", data.getYear()+1900);		

		//OBTENDO RESULTADOS
		return consulta.getResultList();
	}

}
