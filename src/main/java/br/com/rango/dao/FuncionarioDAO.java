package br.com.rango.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import br.com.rango.entities.Estabelecimento;
import br.com.rango.entities.Funcionario;
import br.com.rango.entities.TipoFuncionario;

public class FuncionarioDAO extends DAO {

	private static final long serialVersionUID = -4502387596380887810L;

	public void save(Funcionario obj) throws Exception {
		EntityManager em = getEntityManager();
		try {
			em.persist(obj);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new Exception("Ocorreu algum erro ao tentar salvar o funcionário.", pe);
		}
	}

	public Funcionario update(Funcionario obj) throws Exception {
		EntityManager em = getEntityManager();
		Funcionario resultado = obj;
		try {
			resultado = em.merge(obj);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new Exception("Ocorreu algum erro ao tentar atualizar o funcionário.", pe);
		}
		return resultado;
	}

	public void delete(Funcionario obj) throws Exception {
		EntityManager em = getEntityManager();
		try {
			obj = em.find(Funcionario.class, obj.getIdFuncionario());
			em.remove(obj);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new Exception("Ocorreu algum erro ao tentar remover o funcionário", pe);
		}
	}

	public Funcionario getByID(Long objId) throws Exception {
		EntityManager em = getEntityManager();
		Funcionario resultado = null;
		try {
			resultado = em.find(Funcionario.class, objId);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new Exception("Ocorreu algum erro ao tentar recuperar o funcionário com base no ID.", pe);
		}
		return resultado;
	}

	public Funcionario getByLogin(String login) throws Exception {
		Funcionario funcionarioBusca = null;
		EntityManager em = getEntityManager();
		try {
			//JPQL
			String jpql = "SELECT funcionario FROM Funcionario funcionario "
					+ " WHERE (funcionario.login = :login)";		

			//CRIANDO QUERY
			funcionarioBusca = em.createQuery(jpql, Funcionario.class).setParameter("login", login).getSingleResult();	

		}catch (NoResultException e) {
			funcionarioBusca = null;
		}catch (RuntimeException e) {
			e.printStackTrace();
		}
		return funcionarioBusca;
	}

	public List<Funcionario> getAll(Estabelecimento e) {	
		EntityManager em = getEntityManager();
		//JPQL
		String jpql = "SELECT funcionario FROM Funcionario funcionario INNER JOIN funcionario.estabelecimento e"
				+ " WHERE (e = :estabelecimento) ORDER BY funcionario.tipoFuncionario";		

		//CRIANDO QUERY
		TypedQuery<Funcionario> consulta = em.createQuery(jpql, Funcionario.class).setParameter("estabelecimento", e);		

		//OBTENDO RESULTADOS
		return consulta.getResultList();

	}

	public List<Funcionario> getAllGarcons(Estabelecimento estabelecimento) {
		EntityManager em = getEntityManager();
		//JPQL
		String jpql = "SELECT funcionario FROM Funcionario funcionario INNER JOIN funcionario.estabelecimento e"
				+ " WHERE (e = :estabelecimento AND funcionario.tipoFuncionario = :tipoFuncionario)";		

		//CRIANDO QUERY
		TypedQuery<Funcionario> consulta = em.createQuery(jpql, Funcionario.class).setParameter("estabelecimento", estabelecimento).setParameter("tipoFuncionario", TipoFuncionario.GARCOM);		

		//OBTENDO RESULTADOS
		return consulta.getResultList();
	}

}
