package br.com.rango.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import br.com.rango.entities.Estabelecimento;

public class EstabelecimentoDAO extends DAO {

	private static final long serialVersionUID = 5019889496578676820L;

	public void save(Estabelecimento obj) throws Exception {
		EntityManager em = getEntityManager();
		try {
			em.persist(obj);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new Exception("Ocorreu algum erro ao tentar salvar o estabelecimento.", pe);
		}
	}

	public Estabelecimento update(Estabelecimento obj) throws Exception {
		EntityManager em = getEntityManager();
		Estabelecimento resultado = obj;
		try {
			resultado = em.merge(obj);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new Exception("Ocorreu algum erro ao tentar atualizar o estabelecimento", pe);
		}
		return resultado;
	}

	public void delete(Estabelecimento obj) throws Exception {
		EntityManager em = getEntityManager();
		try {
			obj = em.find(Estabelecimento.class, obj.getId());
			em.remove(obj);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new Exception("Ocorreu algum erro ao tentar remover o estabelecimento.", pe);
		}
	}
	
	public List<Estabelecimento> getAll() {
		EntityManager em = getEntityManager();
		//JPQL
		String jpql = "SELECT estabelecimento FROM Estabelecimento estabelecimento";		

		//CRIANDO QUERY
		TypedQuery<Estabelecimento> consulta = em.createQuery(jpql, Estabelecimento.class);

		//OBTENDO RESULTADOS
		return consulta.getResultList();
	}

	public Estabelecimento getByID(Long objId) throws Exception {
		EntityManager em = getEntityManager();
		Estabelecimento resultado = null;
		try {
			resultado = em.find(Estabelecimento.class, objId);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new Exception("Ocorreu algum erro ao tentar recuperar o estabelecimento com base no ID.", pe);
		}
		return resultado;
	}	

	public Estabelecimento getByName(Estabelecimento e) {
		EntityManager em = getEntityManager();
		Estabelecimento estabelecimento = null;		
		try {
			//JPQL
			String jpql = "SELECT estabelecimento FROM Estabelecimento estabelecimento "
					+ " WHERE (estabelecimento.nome = :nome)";		

			//CRIANDO QUERY
			estabelecimento = em.createQuery(jpql, Estabelecimento.class).setParameter("nome", e.getNome()).getSingleResult();	

		}catch (NoResultException ex) {
			estabelecimento = null;
		}catch (RuntimeException ex) {
			ex.printStackTrace();
		}
		return estabelecimento;		
	}

	public Estabelecimento getByCNPJ(String cnpj) {
		EntityManager em = getEntityManager();
		Estabelecimento estabelecimento = null;

		try {
			//JPQL
			String jpql = "SELECT estabelecimento FROM Estabelecimento estabelecimento "
					+ " WHERE (estabelecimento.CNPJ = :cnpj)";		

			//CRIANDO QUERY
			estabelecimento = em.createQuery(jpql, Estabelecimento.class).setParameter("cnpj", cnpj).getSingleResult();	

		}catch (NoResultException e) {
			estabelecimento = null;
		}catch (RuntimeException e) {
			e.printStackTrace();
		}
		return estabelecimento;		
	}
}
