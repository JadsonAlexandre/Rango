package br.com.rango.dao;

import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import br.com.rango.entities.Estabelecimento;
import br.com.rango.entities.ItemPedido;
import br.com.rango.entities.Pedido;

public class ItemPedidoDAO extends DAO {

	private static final long serialVersionUID = 2410643545008225271L;
	
	public void save(ItemPedido obj) throws Exception {
		EntityManager em = getEntityManager();
		try {
			em.persist(obj);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new Exception("Ocorreu algum erro ao tentar salvar o item do pedido.", pe);
		}
	}
	
	public ItemPedido update(ItemPedido obj) throws Exception {
		EntityManager em = getEntityManager();
		ItemPedido resultado = obj;
		try {
			resultado = em.merge(obj);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new Exception("Ocorreu algum erro ao tentar atualizar o item do pedido.", pe);
		}
		return resultado;
	}
	
	public void delete(ItemPedido obj) throws Exception {
		EntityManager em = getEntityManager();
		try {
			obj = em.find(ItemPedido.class, obj.getId());
			em.remove(obj);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new Exception("Ocorreu algum erro ao tentar remover o item do pedido.", pe);
		}
	}
	
	public ItemPedido getByID(Long objId) throws Exception {
		EntityManager em = getEntityManager();
		ItemPedido resultado = null;
		try {
			resultado = em.find(ItemPedido.class, objId);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new Exception("Ocorreu algum erro ao tentar recuperar o item do pedido com base no ID.", pe);
		}
		return resultado;
	}
	
	public List<ItemPedido> getAll(Estabelecimento e) {
		EntityManager em = getEntityManager();
		//JPQL
		String jpql = "SELECT itemPedido FROM ItemPedido itemPedido INNER JOIN itemPedido.estabelecimento e"
				+ " WHERE (e = :estabelecimento)";		

		//CRIANDO QUERY
		TypedQuery<ItemPedido> consulta = em.createQuery(jpql, ItemPedido.class).setParameter("estabelecimento", e);		

		//OBTENDO RESULTADOS
		return consulta.getResultList();
	}
	
	public List<ItemPedido> getAllByOrder(Pedido pedido) {
		EntityManager em = getEntityManager();
		//JPQL
		String jpql = "SELECT itemPedido FROM ItemPedido itemPedido INNER JOIN itemPedido.pedido pedido"
				+ " WHERE (pedido = :pedido)";		

		//CRIANDO QUERY
		TypedQuery<ItemPedido> consulta = em.createQuery(jpql, ItemPedido.class).setParameter("pedido", pedido);		

		//OBTENDO RESULTADOS
		return consulta.getResultList();
	}
	
	public List<ItemPedido> getAllInKitchen(Estabelecimento e) {
		EntityManager em = getEntityManager();
		//JPQL
		String jpql = "SELECT itemPedido FROM ItemPedido itemPedido INNER JOIN itemPedido.estabelecimento e INNER JOIN itemPedido.produto p"
				+ " WHERE (e = :estabelecimento AND p.daCasa = true AND itemPedido.entregue = false) ORDER BY itemPedido.id";

		//CRIANDO QUERY
		TypedQuery<ItemPedido> consulta = em.createQuery(jpql, ItemPedido.class).setParameter("estabelecimento", e);		

		//OBTENDO RESULTADOS
		return consulta.getResultList();
	}

	public List<ItemPedido> getAllByDate(Estabelecimento estabelecimento, Date date) {
		EntityManager em = getEntityManager();
		//JPQL
		String jpql = "SELECT itemPedido FROM ItemPedido itemPedido INNER JOIN itemPedido.estabelecimento e INNER JOIN itemPedido.pedido p"
				+ " WHERE (e = :estabelecimento AND p.valorPago != 0 AND month(p.dataAbertura) = :mes AND year(p.dataAbertura) = :ano)";		
		
		
		//CRIANDO QUERY
		TypedQuery<ItemPedido> consulta = em.createQuery(jpql, ItemPedido.class).setParameter("estabelecimento", estabelecimento).setParameter("mes", date.getMonth()+1).setParameter("ano", date.getYear()+1900);		

		//OBTENDO RESULTADOS
		return consulta.getResultList();
	}

}
