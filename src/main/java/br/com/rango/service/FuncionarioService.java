package br.com.rango.service;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import br.com.rango.dao.EstabelecimentoDAO;
import br.com.rango.dao.FuncionarioDAO;
import br.com.rango.dao.PedidoDAO;
import br.com.rango.entities.Estabelecimento;
import br.com.rango.entities.Funcionario;
import br.com.rango.entities.Pedido;
import br.com.rango.entities.TipoFuncionario;
import br.com.rango.entities.TopGarcom;
import br.com.rango.util.TransacionalCdi;

public class FuncionarioService implements Serializable{	

	private static final long serialVersionUID = 9153093174043225369L;

	@Inject
	private FuncionarioDAO funcionarioDAO;	

	@Inject
	private EstabelecimentoDAO estabelecimentoDAO;	
	
	@Inject
	private PedidoDAO pedidoDAO;	

	public FuncionarioDAO getFuncionarioDAO() {
		return funcionarioDAO;
	}
	public void setFuncionarioDAO(FuncionarioDAO funcionarioDAO) {
		this.funcionarioDAO = funcionarioDAO;
	}
	public EstabelecimentoDAO getEstabelecimentoDAO() {
		return estabelecimentoDAO;
	}
	public void setEstabelecimentoDAO(EstabelecimentoDAO estabelecimentoDAO) {
		this.estabelecimentoDAO = estabelecimentoDAO;
	}
	public PedidoDAO getPedidoDAO() {
		return pedidoDAO;
	}
	public void setPedidoDAO(PedidoDAO pedidoDAO) {
		this.pedidoDAO = pedidoDAO;
	}

	@TransacionalCdi
	public String cadastrarFuncionario(Funcionario funcionario) throws Exception {	
		try {
			if(buscarFuncionarioLogin(funcionario.getLogin())==null) {		
				validarSenha(funcionario.getSenha());
				calcularHashDaSenha(funcionario);
				funcionarioDAO.save(funcionario);			
				return "Funcionário cadastrado com sucesso!";
			}
			else {
				throw new Exception("Já existe um funcionário cadastrado com esse login!");
			}			
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	public List<Funcionario> buscarTodosFuncionarios(Estabelecimento estabelecimento) throws Exception {
		try {
			return funcionarioDAO.getAll(estabelecimento);			
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}		
	}

	public Funcionario buscarFuncionario(Funcionario funcionario) throws Exception {
		try {
			return funcionarioDAO.getByID(funcionario.getIdFuncionario());
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}		
	}

	@TransacionalCdi
	public String atualizarFuncionario(Funcionario funcionario) throws Exception {	
		try {
			if(validarAtualizacao(funcionario)) {				
				funcionarioDAO.update(funcionario);				
				return "Funcionário atualizado com sucesso!";
			}
			else {
				throw new Exception("Já existe um funcionário cadastrado com esse login!");
			}				
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}			
	}

	@TransacionalCdi
	public String atualizarSenhaFuncionario(Funcionario funcionario) throws Exception {	
		try {
			if(validarAtualizacao(funcionario)) {	
				validarSenha(funcionario.getSenha());
				calcularHashDaSenha(funcionario);	
				funcionarioDAO.update(funcionario);				
				return "Senha atualizada com sucesso!";
			}
			else {
				throw new Exception("Já existe um funcionário cadastrado com esse login!");
			}		
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}			
	}

	@TransacionalCdi
	public String removerFuncionario(Funcionario funcionario) throws Exception {
		try {
			if(funcionario.getTipoFuncionario()!=TipoFuncionario.GERENTE) {			
				funcionarioDAO.delete(funcionario);			
				return "Funcionario removido com sucesso!";			
			}
			else {
				throw new Exception("Você não pode remover um Gerente!");
			}			
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}	

	public Funcionario buscarFuncionarioLogin(String login) throws Exception {	
		try {
			return funcionarioDAO.getByLogin(login);			
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}					
	}	

	public boolean validarAtualizacao(Funcionario funcionario) throws Exception {
		Funcionario funcionarioBusca = buscarFuncionarioLogin(funcionario.getLogin());
		if(funcionarioBusca==null) {
			return true;
		}
		else if (funcionarioBusca!=null && funcionarioBusca.getIdFuncionario()==funcionario.getIdFuncionario()) {
			return true;
		}
		return false;		
	}

	public String calcularHashDaSenha(Funcionario funcionario) throws Exception {
		funcionario.setSenha(hash(funcionario.getSenha()));
		return funcionario.getSenha();
	}

	public String hash(String password) throws Exception {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("SHA-256");
			md.update(password.getBytes("UTF-8"));
			byte[] digest = md.digest();
			//BigInteger bigInt = new BigInteger(1, digest);
			//String output = bigInt.toString(16);
			String output = Base64.getEncoder().encodeToString(digest);
			return output;
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			throw new Exception("Could not calculate hash!", e);
		}
	}

	@TransacionalCdi
	public void cadastroInicial(Funcionario funcionario, Estabelecimento estabelecimento) throws Exception {
		try {
			if(buscarFuncionarioLogin(funcionario.getLogin())==null) {
				if(estabelecimento.getNome().length()>1 && estabelecimentoDAO.getByName(estabelecimento)==null) {			
					estabelecimentoDAO.save(estabelecimento);
					validarSenha(funcionario.getSenha());
					calcularHashDaSenha(funcionario);
					funcionarioDAO.update(funcionario);	
					EmailService emailService = new EmailService();
					emailService.enviarEmailConclusaoCadastro(funcionario.getEmail(), funcionario.getNome());
				}
				else {
					throw new Exception("Este nome é inválido ou já existe um estabelecimento cadastrado com esse nome");
				}						
			}
			else {
				throw new Exception("Já existe um funcionário cadastrado com esse login!");				
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}		
	}

	public List<TopGarcom> buscarTopGarcons(Estabelecimento estabelecimento, Date data) throws Exception{
		try {
			List<TopGarcom> topGarcons = new ArrayList<TopGarcom>();
			List<TopGarcom> topGarconsFiltrados = new ArrayList<TopGarcom>();

			//PEGANDO TODOS OS GARÇONS
			List<Funcionario> garcons = funcionarioDAO.getAllGarcons(estabelecimento);
			
			//TRANSFORMANDO O OBJETO E COLOCANDO ELES EM UMA LISTA
			for(Funcionario f : garcons) {
				TopGarcom top = new TopGarcom();
				f.setPedidos(pedidoDAO.getOrderByEmployeeInDate(f,data));
				top.setNome(f.getNome());
				top.setPedidos(f.getPedidos().size());
				float valor = 0;
				for(Pedido p : f.getPedidos()) {
					valor+= p.getValorPago();
				}				
				top.setValor(valor);
				topGarcons.add(top);				
			}

			//BUSCANDO OS 3 MELHORES
			int primeiro = -1, segundo = -1, terceiro = -1; 
			float valorPrimeiro = -1, valorSegundo = -1, valorTerceiro = -1; 
			for(TopGarcom t: topGarcons) {
				//CASO TOP 1
				if(t.getValor()>valorPrimeiro) {
					//REBAIXANDO SEGUNDO
					terceiro = segundo;
					valorTerceiro = valorSegundo;
					//REBAIXANDO PRIMEIRO
					segundo = primeiro;
					valorSegundo = valorPrimeiro;
					//SETANDO PRIMEIRO
					primeiro = topGarcons.indexOf(t);
					valorPrimeiro = t.getValor();
				}
				else if(t.getValor()>valorSegundo) {
					//REBAIXANDO SEGUNDO
					terceiro = segundo;
					valorTerceiro = valorSegundo;
					//SETANDO SEGUNDO
					segundo = topGarcons.indexOf(t);
					valorSegundo = t.getValor();
				}
				else if(t.getValor()>valorTerceiro) {
					//SETANDO TERCEIRO
					terceiro = topGarcons.indexOf(t);
					valorTerceiro = t.getValor();
				}
			}

			//ADICIONANDO OS TOPS AO ARRAY FINAL
			if(primeiro!=-1) {
				topGarconsFiltrados.add(topGarcons.get(primeiro));
			}
			if(segundo!=-1) {
				topGarconsFiltrados.add(topGarcons.get(segundo));				
			}
			if(terceiro!=-1) {
				topGarconsFiltrados.add(topGarcons.get(terceiro));				
			}
			return topGarconsFiltrados;
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}

	}

	public void validarSenha(String senha) throws Exception {
		if(senha.length()<6) {
			throw new Exception("A senha deve conter no mínimo 6 dígitos");
		}

		int numeros = 0;
		int letras = 0;
		for(int i=0; i<senha.length(); i++){
			Character letra = senha.charAt(i);
			try{
				Integer.valueOf(letra.toString());
				numeros++;
			}catch (Exception e) {
				letras++;
				continue;
			}
		}
		if(numeros < 3 || letras < 3){
			throw new Exception("A senha deve conter 3 letras e 3 números");
		}

	}
}
