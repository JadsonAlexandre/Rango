package br.com.rango.service;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;
import br.com.rango.dao.CadastroDAO;
import br.com.rango.dao.FuncionarioDAO;
import br.com.rango.entities.Cadastro;
import br.com.rango.entities.Funcionario;
import br.com.rango.entities.TipoFuncionario;
import br.com.rango.util.TransacionalCdi;

public class CadastroService implements Serializable{

	private static final long serialVersionUID = 2836043946574616904L;	

	@Inject
	private CadastroDAO cadastroDAO;
	
	@Inject
	private FuncionarioDAO funcionarioDAO;

	@Inject
	private EmailService emailService;
	
	
	public CadastroDAO getCadastroDAO() {
		return cadastroDAO;
	}
	public void setCadastroDAO(CadastroDAO cadastroDAO) {
		this.cadastroDAO = cadastroDAO;
	}
	public EmailService getEmailService() {
		return emailService;
	}
	public void setEmailService(EmailService emailService) {
		this.emailService = emailService;
	}	
	
	
	@TransacionalCdi
	public String cadastrarGerente(Cadastro cadastro) throws Exception{
		try {			
			cadastroDAO.save(cadastro);
			emailService.enviarEmailConfirmacaoCadastro(cadastro.getEmail(), cadastro.getNome());
			return "/obrigado";
		} catch (Exception e) {
			throw new Exception(e.getMessage(), e);
		}				
	}

	
	public List<Cadastro> buscarTodosCadastros() throws Exception{
		try {
			return cadastroDAO.getAll();			
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}		
	}

	
	public Cadastro buscarCadastro(Long id) throws Exception {
		try {
			return cadastroDAO.getByID(id);			
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}		
	}

	@TransacionalCdi
	public String atualizarCadastro(Cadastro cadastro) throws Exception {
		try {
			cadastroDAO.update(cadastro);
			return null;
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}		
	}

	@TransacionalCdi
	public String removerCadastro(Cadastro cadastro) throws Exception {
		try {
			cadastroDAO.delete(cadastro);	
			return null;
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}				
	}

	@TransacionalCdi
	public String confirmarPagamentoCadastro(Cadastro cadastro) throws Exception {
		try {
			//VERIFICANDO SE JÁ FOI PAGO
			if(cadastro.isPago()) {
				throw new Exception("Esse cadastro já foi pago!");
			}

			//ATUALIZANDO CADASTRO
			cadastro.setPago(true);
			atualizarCadastro(cadastro);

			//CRIAR FUNCIONARIO GERENTE
			Funcionario funcionario = converterCadastroFuncionario(cadastro);

			//MANDAR EMAIL COM CONFIRMAÇÃO JUNTO COM LOGIN E SENHA			
			emailService.enviarEmailConfirmacaoPagamentoCadastro(cadastro.getEmail(), cadastro.getNome(), funcionario.getLogin(), funcionario.getSenha());
			
			//SALVANDO FUNCIONARIO GERENTE NO BANCO	
			calcularHashDaSenha(funcionario);
			funcionarioDAO.save(funcionario);		
			return null;

		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}

	}

	private Funcionario converterCadastroFuncionario(Cadastro cadastro) {		
		Funcionario funcionario = new Funcionario();
		funcionario.setNome(cadastro.getNome());		
		funcionario.setTelefone(cadastro.getTelefone());
		funcionario.setEmail(cadastro.getEmail());
		funcionario.setTipoFuncionario(TipoFuncionario.GERENTE);
		//GERAR LOGIN E SENHA
		funcionario.setLogin(gerarLogin(funcionario.getNome()));
		funcionario.setSenha(gerarSenha());
		return funcionario;
	}

	private String gerarSenha() {
		UUID uuid = UUID.randomUUID();  
		String senha = uuid.toString();  
		return senha.substring(0,6);
	}

	private String gerarLogin(String nome) {
		String pattern = "\\S+";
		String login = null;	
		Pattern r = Pattern.compile(pattern);
		Matcher m = r.matcher(nome);
		if (m.find( )) {
			login = m.group(0).toLowerCase();
		}		
		UUID uuid = UUID.randomUUID();  
		String myRandom = uuid.toString();
		login+=myRandom.substring(0,3);
		return login;
	}
	
	private String calcularHashDaSenha(Funcionario funcionario) throws Exception {
		funcionario.setSenha(hash(funcionario.getSenha()));
		return funcionario.getSenha();
	}

	private String hash(String password) throws Exception {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("SHA-256");
			md.update(password.getBytes("UTF-8"));
			byte[] digest = md.digest();
			//BigInteger bigInt = new BigInteger(1, digest);
			//String output = bigInt.toString(16);
			String output = Base64.getEncoder().encodeToString(digest);
			return output;
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			throw new Exception("Could not calculate hash!", e);
		}
	}
}
