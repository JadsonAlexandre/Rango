package br.com.rango.service;

import java.net.MalformedURLException;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;

public class EmailService {
	
	public void enviarEmailConfirmacaoCadastro(String destinatario, String nome) throws EmailException, MalformedURLException {
		HtmlEmail email = new HtmlEmail();		
		email.setHostName("smtp.gmail.com");		
		email.setSmtpPort(587);
		email.setAuthenticator(new DefaultAuthenticator("gerencia.rango@gmail.com", "1826studiojs"));		
		email.setStartTLSEnabled(true);
		email.setFrom("gerencia.rango@gmail.com","Rango!");		
		email.setSubject("Solicitação de Cadastro");		
		email.addTo(destinatario);				
		email.setHtmlMsg(
		"<html>"+
		"<head>"+
				"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />"+
				"<title>Rango!</title>"+
		"</head>"+
		"<body style=\"margin: 0; padding: 0\">"+
			"<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"border-collapse: collapse;\">"+
				"<tr>"+	
					"<td align=\"center\" bgcolor=\"#D12034\" style=\"padding: 5px 0 5px 0;\">"+
						"<img src=\"https://docs.google.com/uc?export=download&id=1djEe8JgiGIdwnXerg7_evyn6XYRjw_FR\" alt=\"Logo Rango!\" width=\"150px\" style=\"display: block;\" />"+
					"</td>"+
				"</tr>"+
				"<tr>"+
					"<td bgcolor=\"#ffffff\" style=\"padding: 40px 30px 40px 30px;\">"+
						"<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">"+
							"<tr>"+
								"<td style=\"color:#1A2226; font-size: 24px; font-family: Arial, sans-serif\" >"+
									"<b>"+nome+"</b>, obrigado por solicitar seu cadastro!"+
								"</td>"+
							"</tr>"+
							"<tr>"+
								"<td style=\"padding: 20px 0 30px 0; color:#1A2226; font-size: 16px; font-family: Arial, sans-serif; line-height: 20px\">"+
									"Em poucos minutos entraremos em contato com você para juntos configurarmos o plano de uso ideal para o seu estabelecimento, \r\n" + 
									"dispondo os recursos e as formas de pagamento. Após confirmado o pagamento, faremos o passo a passo inicial com seus dados e do seu \r\n" + 
									"estabelecimento. Até daqui a pouco :)"+
								"</td>"+
							"</tr>"+
						"</table>"+
					"</td>"+
				"</tr>"+
			"</table>"+
		"</body>"+
		"</html>");
		
		email.setTextMsg("Seu servidor de email não suporta mensagem HTML");		
		email.send();
	}
	
	public void enviarEmailConfirmacaoPagamentoCadastro(String destinatario, String nome, String login, String senha) throws EmailException, MalformedURLException {
		HtmlEmail email = new HtmlEmail();		
		email.setHostName("smtp.gmail.com");		
		email.setSmtpPort(587);
		email.setAuthenticator(new DefaultAuthenticator("gerencia.rango@gmail.com", "1826studiojs"));		
		email.setStartTLSEnabled(true);
		email.setFrom("gerencia.rango@gmail.com","Rango!");		
		email.setSubject("Confirmação de Pagamento");		
		email.addTo(destinatario);				
		email.setHtmlMsg(
		"<html>"+
		"<head>"+
				"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />"+
				"<title>Rango!</title>"+
		"</head>"+
		"<body style=\"margin: 0; padding: 0\">"+
			"<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"border-collapse: collapse;\">"+
				"<tr>"+	
					"<td align=\"center\" bgcolor=\"#D12034\" style=\"padding: 5px 0 5px 0;\">"+
						"<img src=\"https://docs.google.com/uc?export=download&id=1djEe8JgiGIdwnXerg7_evyn6XYRjw_FR\" alt=\"Logo Rango!\" width=\"150px\" style=\"display: block;\" />"+
					"</td>"+
				"</tr>"+
				"<tr>"+
					"<td bgcolor=\"#ffffff\" style=\"padding: 40px 30px 40px 30px;\">"+
						"<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">"+
							"<tr>"+
								"<td style=\"color:#1A2226; font-size: 24px; font-family: Arial, sans-serif\" >"+
									"<b>"+nome+"</b>, seu pagamento foi confirmado!"+
								"</td>"+
							"</tr>"+
							"<tr>"+
								"<td style=\"padding: 20px 0 30px 0; color:#1A2226; font-size: 16px; font-family: Arial, sans-serif; line-height: 20px\">"+
									"Está quase tudo pronto! Agora só falta configurarmos os seus dados e os dados do seu estabelecimento. Para isso, acesse a aba 'Login' \r\n" + 
									"na nossa página inicial e entre no sistema usando o login e senha dispostos logo abaixo. Não se preocupe com esses dados pois eles serão \r\n" + 
									"usados apenas para esse primero acesso. Qualquer dúvida pode nos mandar respondendo esse email, que iremos te responder o mais rápido possível :)"+
								"</td>"+
							"</tr>"+
							"<tr>"+
								"<td style=\"color:#1A2226; font-size: 16px; font-family: Arial, sans-serif\" >"+
									"Login: <b>"+login+"</b><br/>"+
									"Senha: <b>"+senha+"</b>"+
								"</td>"+
							"</tr>"+
						"</table>"+
					"</td>"+
				"</tr>"+
			"</table>"+
		"</body>"+
		"</html>");
		
		email.setTextMsg("Seu servidor de email não suporta mensagem HTML");		
		email.send();
	}	
	
	public void enviarEmailConclusaoCadastro(String destinatario, String nome) throws EmailException, MalformedURLException {
		HtmlEmail email = new HtmlEmail();		
		email.setHostName("smtp.gmail.com");		
		email.setSmtpPort(587);
		email.setAuthenticator(new DefaultAuthenticator("gerencia.rango@gmail.com", "1826studiojs"));		
		email.setStartTLSEnabled(true);
		email.setFrom("gerencia.rango@gmail.com","Rango!");		
		email.setSubject("Conclusão de Cadastro");		
		email.addTo(destinatario);				
		email.setHtmlMsg(
		"<html>"+
		"<head>"+
				"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />"+
				"<title>Rango!</title>"+
		"</head>"+
		"<body style=\"margin: 0; padding: 0\">"+
			"<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"border-collapse: collapse;\">"+
				"<tr>"+	
					"<td align=\"center\" bgcolor=\"#D12034\" style=\"padding: 5px 0 5px 0;\">"+
						"<img src=\"https://docs.google.com/uc?export=download&id=1djEe8JgiGIdwnXerg7_evyn6XYRjw_FR\" alt=\"Logo Rango!\" width=\"150px\" style=\"display: block;\" />"+
					"</td>"+
				"</tr>"+
				"<tr>"+
					"<td bgcolor=\"#ffffff\" style=\"padding: 40px 30px 40px 30px;\">"+
						"<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">"+
							"<tr>"+
								"<td style=\"color:#1A2226; font-size: 24px; font-family: Arial, sans-serif\" >"+
									"<b>"+nome+"</b>, obrigado por cadastrar-se!"+
								"</td>"+
							"</tr>"+
							"<tr>"+
								"<td style=\"padding: 20px 0 30px 0; color:#1A2226; font-size: 16px; font-family: Arial, sans-serif; line-height: 20px\">"+
									"Está tudo pronto! A partir de agora você poderá desfrutar das funcionalidades que o Rango! pode te oferecer. \r\n" + 
									"Qualquer dúvida pode nos contatar através desse endereço de email ou através do telefone (83) 9 9804-0556. \r\n" + 
									"Faça seu login nosso site agora e comece a usar o Rango! :)"+
								"</td>"+
							"</tr>"+
						"</table>"+
					"</td>"+
				"</tr>"+
			"</table>"+
		"</body>"+
		"</html>");
		
		email.setTextMsg("Seu servidor de email não suporta mensagem HTML");		
		email.send();
	}

}
