package br.com.rango.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.inject.Inject;
import br.com.rango.dao.ProdutoDAO;
import br.com.rango.dao.TipoProdutoDAO;
import br.com.rango.entities.Estabelecimento;
import br.com.rango.entities.Produto;
import br.com.rango.entities.TipoProduto;
import br.com.rango.util.TransacionalCdi;

public class TipoProdutoService implements Serializable {	
	
	private static final long serialVersionUID = -7110134662398905911L;
	
	@Inject
	private TipoProdutoDAO tipoProdutoDAO;
	
	@Inject
	private ProdutoDAO produtoDAO;
	
	public TipoProdutoDAO getTipoProdutoDAO() {
		return tipoProdutoDAO;
	}
	public void setTipoProdutoDAO(TipoProdutoDAO tipoProdutoDAO) {
		this.tipoProdutoDAO = tipoProdutoDAO;
	}

	@TransacionalCdi
	public String cadastrarTipo(TipoProduto tipo) throws Exception {
		try {
			if(buscarTipoPorNome(tipo)==null) {			
				tipoProdutoDAO.save(tipo);			
				return "Categoria cadastrada com sucesso!";			
			}
			else {
				throw new Exception("Já existe uma categoria cadastrada com esse nome");
			}			
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}		
	}

	public Collection<TipoProduto> buscarTodosTipos(Estabelecimento estabelecimento) throws Exception {
		try {
			List<TipoProduto> tipos = tipoProdutoDAO.getAll(estabelecimento);		
			if(tipos.size()>0) {
				return tipos;
			}
			else{
				return new ArrayList<TipoProduto>();
			}			
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}		
	}

	public TipoProduto buscarTipoPorNome(TipoProduto tipo) throws Exception {
		try {
			return tipoProdutoDAO.getByName(tipo);			
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}		
	}
	
	public TipoProduto buscarPorID(Long id) throws Exception {
		try {			
			return tipoProdutoDAO.getByID(id);
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}		
	}

	@TransacionalCdi
	public String atualizarTipo(TipoProduto tipo) throws Exception {
		try {
			if(validarAtualizacao(tipo)) {			
				tipoProdutoDAO.update(tipo);			
				return "Categoria atualizada com sucesso!";
			}
			else {
				throw new Exception("Já existe uma categoria com esse nome!");
			}			
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}		
	}
	
	@TransacionalCdi
	public String removerTipo(TipoProduto tipo) throws Exception {
		try {
			List<Produto> produtos = produtoDAO.getAllByType(tipo);
			for(Produto p : produtos) {
				produtoDAO.delete(p);
			}
			tipoProdutoDAO.delete(tipo);		
			return "Categoria removida com sucesso!";			
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}		
	}

	private boolean validarAtualizacao(TipoProduto tipo) throws Exception {
		TipoProduto tipoBusca = buscarTipoPorNome(tipo);
		if(tipoBusca==null) {
			return true;
		}
		else if (tipoBusca!=null && tipoBusca.getId()==tipo.getId()) {
			return true;
		}
		return false;
	}
}
