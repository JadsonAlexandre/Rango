package br.com.rango.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import br.com.rango.dao.MesaDAO;
import br.com.rango.dao.PedidoDAO;
import br.com.rango.entities.Estabelecimento;
import br.com.rango.entities.Pedido;
import br.com.rango.util.TransacionalCdi;

public class PedidoService implements Serializable {	
	
	private static final long serialVersionUID = -5952781836733793335L;
	
	@Inject
	private PedidoDAO pedidoDAO;
	
	@Inject
	private MesaDAO mesaDAO;	
	

	public PedidoDAO getPedidoDAO() {
		return pedidoDAO;
	}
	public void setPedidoDAO(PedidoDAO pedidoDAO) {
		this.pedidoDAO = pedidoDAO;
	}
	public MesaDAO getMesaDAO() {
		return mesaDAO;
	}
	public void setMesaDAO(MesaDAO mesaDAO) {
		this.mesaDAO = mesaDAO;
	}

	@TransacionalCdi
	public String cadastrarPedido(Pedido pedido) throws Exception {
		try {
			if(!pedido.getMesa().isOcupada()) {
				pedido.getMesa().setOcupada(true);			
				mesaDAO.update(pedido.getMesa());
				pedidoDAO.save(pedido);			
				return "Pedido criado com sucesso!";
			}
			else {			
				throw new Exception("Ops! Essa mesa já está ocupada");			
			}			
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}		
	}

	public Collection<Pedido> buscarTodosPedidosAbertos(Estabelecimento estabelecimento) throws Exception {
		try {
			List<Pedido> pedidos = pedidoDAO.getAllOpen(estabelecimento);		
			if(pedidos.size()>0) {
				return pedidos;
			}
			else {
				return new ArrayList<Pedido>();
			}			
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}		
	}
	
	public Collection<Pedido> buscarTodosPedidosFechados(Estabelecimento estabelecimento) throws Exception {
		try {
			List<Pedido> pedidos = pedidoDAO.getAllClosed(estabelecimento);		
			if(pedidos.size()>0) {
				return pedidos;
			}
			else {
				return new ArrayList<Pedido>();
			}			
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}		
	}
	
	public Collection<Pedido> buscarTodosPedidosMes(Estabelecimento estabelecimento) throws Exception {
		try {
			List<Pedido> pedidos = pedidoDAO.getAllByMoth(estabelecimento, new Date(System.currentTimeMillis()));		
			if(pedidos.size()>0) {
				return pedidos;
			}
			else {
				return new ArrayList<Pedido>();
			}			
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}		
	}

	public Pedido buscarPorId(Pedido pedidoSelecionado) throws Exception {
		try {
			return pedidoDAO.getByID(pedidoSelecionado.getIdPedido());					
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}		
	}
	
	@TransacionalCdi
	public String atualizarPedido(Pedido pedido) throws Exception {
		try {
			pedidoDAO.update(pedido);
			return "Pedido atualizado com sucesso!";
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	@TransacionalCdi
	public String removerPedido(Pedido pedido) throws Exception {
		try {			
			pedido.getMesa().setOcupada(false);
			mesaDAO.update(pedido.getMesa());
			pedidoDAO.delete(pedido);
			return "Pedido atualizado com sucesso!";
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	
}
