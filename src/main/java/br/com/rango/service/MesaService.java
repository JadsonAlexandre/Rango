package br.com.rango.service;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.inject.Inject;
import br.com.rango.dao.MesaDAO;
import br.com.rango.entities.Estabelecimento;
import br.com.rango.entities.Mesa;
import br.com.rango.util.TransacionalCdi;

public class MesaService implements Serializable {	

	private static final long serialVersionUID = 3151704709932956837L;

	@Inject
	private MesaDAO mesaDAO;		

	public MesaDAO getMesaDAO() {
		return mesaDAO;
	}
	public void setMesaDAO(MesaDAO mesaDAO) {
		this.mesaDAO = mesaDAO;
	}

	@TransacionalCdi
	public String cadastrarMesa(Mesa mesa) throws Exception {
		try {
			if(buscarMesaPorNumero(mesa)==null) {				
				mesaDAO.save(mesa);				
				return "Mesa cadastrada com sucesso!";			
			}
			else {
				throw new Exception("Já existe uma mesa cadastrada com esse número");
			}	

		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}				
	}

	public Collection<Mesa> buscarTodasMesas(Estabelecimento estabelecimento) throws Exception{
		try {
			List<Mesa> mesas = mesaDAO.getAll(estabelecimento);
			if(mesas.size()>0) {			
				return mesas;
			}
			else{
				return new ArrayList<Mesa>();
			}			
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	public Mesa buscarMesaPorNumero(Mesa mesa) throws Exception {
		try {
			return mesaDAO.getByNumber(mesa);
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	@TransacionalCdi
	public String atualizarMesa(Mesa mesa) throws Exception {
		try {
			if(validarAtualizacao(mesa)) {			
				mesaDAO.update(mesa);			
				return "Mesa atualizada com sucesso!";
			}
			else {
				throw new Exception("Já existe uma mesa com esse número!");
			}			
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}		
	}

	private boolean validarAtualizacao(Mesa mesa) throws Exception {
		Mesa mesaBusca = buscarMesaPorNumero(mesa);
		if(mesaBusca==null) {
			return true;
		}
		else if (mesaBusca!=null && mesaBusca.getId()==mesa.getId()) {
			return true;
		}
		return false;
	}

	@TransacionalCdi
	public String removerMesa(Mesa mesa) throws Exception {
		try {
			if(!mesa.isOcupada()) {				
				mesaDAO.delete(mesa);				
				return "Mesa removida com sucesso!";
			}
			else {
				return "Você não pode remover uma mesa que está ocupada!";
			}			
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
		
	}
}
