package br.com.rango.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import br.com.rango.dao.ItemPedidoDAO;
import br.com.rango.dao.ProdutoDAO;
import br.com.rango.entities.Estabelecimento;
import br.com.rango.entities.ItemPedido;
import br.com.rango.entities.Pedido;
import br.com.rango.entities.Produto;
import br.com.rango.entities.TopProduto;
import br.com.rango.util.TransacionalCdi;

public class ItemPedidoService implements Serializable{

	private static final long serialVersionUID = 8509536473944763991L;	

	@Inject
	private ItemPedidoDAO itemPedidoDAO;

	@Inject
	private ProdutoDAO produtoDAO;


	public ItemPedidoDAO getItemPedidoDAO() {
		return itemPedidoDAO;
	}
	public void setItemPedidoDAO(ItemPedidoDAO itemPedidoDAO) {
		this.itemPedidoDAO = itemPedidoDAO;
	}
	public ProdutoDAO getProdutoDAO() {
		return produtoDAO;
	}
	public void setProdutoDAO(ProdutoDAO produtoDAO) {
		this.produtoDAO = produtoDAO;
	}


	@TransacionalCdi
	public String adicionarItem(ItemPedido item) throws Exception {
		if(item.getQuantidade()>0) {
			try {
				Produto produto = produtoDAO.getByID(item.getProduto().getId());
				if(produto!=null && produto.getQuantidade()!=null && item.getQuantidade()<=produto.getQuantidade()) {			
					itemPedidoDAO.save(item);			
					produto.setQuantidade(produto.getQuantidade()-item.getQuantidade());
					produtoDAO.update(produto);
					return "Item adicionado com sucesso!";
				}
				else if(produto.isDaCasa()) {
					itemPedidoDAO.save(item);	
					return "Item adicionado com sucesso!";					
				}
				else {			
					throw new Exception("Ops! O produto não existe ou não possui quantidade o suficiente");
				}			
			} catch (Exception e) {
				throw new Exception(e.getMessage());
			}
		}
		else {
			throw new Exception("Digite uma quantidade válida!");			
		}
	}

	@TransacionalCdi
	public String removerItem(ItemPedido item) throws Exception {
		try {
			Produto produto = produtoDAO.getByID(item.getProduto().getId());
			if(!produto.isDaCasa()) {
				produto.setQuantidade(produto.getQuantidade()+item.getQuantidade());
				produtoDAO.update(produto);
			}
			itemPedidoDAO.delete(item);				
			return "Item removido com sucesso!";

		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	@TransacionalCdi
	public String atualizarItem(ItemPedido item) throws Exception {
		if(item.getQuantidade()>0) {
			try {
				Produto produto = produtoDAO.getByID(item.getProduto().getId());
				ItemPedido itemAntigo = itemPedidoDAO.getByID(item.getId());
				if(!produto.isDaCasa()) {
					int diferencaQuantidade = item.getQuantidade() - itemAntigo.getQuantidade();
					validarQuantidade(produto, diferencaQuantidade);
					produto.setQuantidade(produto.getQuantidade()-(diferencaQuantidade));
					produtoDAO.update(produto);					
				}				
				itemPedidoDAO.update(item);
				return "Item atualizado com sucesso!";			
			} catch (Exception e) {
				throw new Exception(e.getMessage());
			}
		}
		else {
			throw new Exception("Digite uma quantidade válida!");			
		}
	}
	
	public List<ItemPedido> buscarTodos(Estabelecimento estabelecimento) throws Exception{
		try {
			List<ItemPedido> itensPedido = itemPedidoDAO.getAll(estabelecimento);		
			if(itensPedido.size()>0) {
				return itensPedido;
			}
			else {
				return new ArrayList<ItemPedido>();
			}	
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	public List<ItemPedido> buscarTodosDoPedido(Pedido pedido) throws Exception{
		try {
			List<ItemPedido> itensPedido = itemPedidoDAO.getAllByOrder(pedido);		
			if(itensPedido.size()>0) {
				return itensPedido;
			}
			else {
				return new ArrayList<ItemPedido>();
			}	
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	public List<ItemPedido> buscarTodosCozinha(Estabelecimento estabelecimento) throws Exception{
		try {
			List<ItemPedido> itensPedido = itemPedidoDAO.getAllInKitchen(estabelecimento);		
			if(itensPedido.size()>0) {
				return itensPedido;
			}
			else {
				return new ArrayList<ItemPedido>();
			}	
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	private void validarQuantidade(Produto produto, int diferencaQuantidade) throws Exception {
		if(produto.getQuantidade()<diferencaQuantidade) {
			throw new Exception("Não há quantidade suficiente desse produto em estoque");			
		}		
	}
	
	public List<TopProduto> buscarTopProdutos(Estabelecimento estabelecimento, Date date) throws Exception {
		try {
			List<TopProduto> topProdutos = new ArrayList<TopProduto>();
			List<TopProduto> topProdutosFiltrados = new ArrayList<TopProduto>();
			
			//PEGANDOS TODOS OS ITENS DE PEDIDO POR UM PERÍODO
			List<ItemPedido> itens = itemPedidoDAO.getAllByDate(estabelecimento, date);
			
			//TRANSFORMANDO O OBJETO E COLOCANDO EM UMA LISTA
			for(ItemPedido i: itens) {
				TopProduto top = new TopProduto();
				top.setNome(i.getProduto().getNome());
				top.setQuantidade(i.getQuantidade());
				top.setLucro((i.getProduto().getValorVenda().doubleValue()-i.getProduto().getValorCusto().doubleValue())*i.getQuantidade());
				if(topProdutos.size()==0) {
					topProdutos.add(top);
				}
				else if(topProdutos.size()>0) {
					boolean repetido = false;
					int idRepetido = -1;
					for(TopProduto t: topProdutos) {
						if(t.getNome().equals(top.getNome())) {
							repetido = true;
							idRepetido = topProdutos.indexOf(t);
						}						
					}
					if(repetido){
						topProdutos.get(idRepetido).setLucro(topProdutos.get(idRepetido).getLucro()+top.getLucro());
						topProdutos.get(idRepetido).setQuantidade(topProdutos.get(idRepetido).getQuantidade()+top.getQuantidade());
					}
					else {
						topProdutos.add(top);	
						//break;
						//continue;
					}
				}
			}
			
			//BUSCANDO OS 3 MELHORES
			int primeiro = -1, segundo = -1, terceiro = -1; 
			double valorPrimeiro = -1, valorSegundo = -1, valorTerceiro = -1;
			for(TopProduto t: topProdutos) {
				//CASO TOP 1
				if(t.getLucro()>valorPrimeiro) {
					//REBAIXANDO SEGUNDO
					terceiro = segundo;
					valorTerceiro = valorSegundo;
					//REBAIXANDO PRIMEIRO
					segundo = primeiro;
					valorSegundo = valorPrimeiro;
					//SETANDO PRIMEIRO
					primeiro = topProdutos.indexOf(t);
					valorPrimeiro = t.getLucro();
				}
				else if(t.getLucro()>valorSegundo) {
					//REBAIXANDO SEGUNDO
					terceiro = segundo;
					valorTerceiro = valorSegundo;
					//SETANDO SEGUNDO
					segundo = topProdutos.indexOf(t);
					valorSegundo = t.getLucro();
				}
				else if(t.getLucro()>valorTerceiro) {
					//SETANDO TERCEIRO
					terceiro = topProdutos.indexOf(t);
					valorTerceiro = t.getLucro();
				}
			}
			
			//ADICIONANDO OS TOPS AO ARRAY FINAL
			if(primeiro!=-1) {
				topProdutosFiltrados.add(topProdutos.get(primeiro));
			}
			if(segundo!=-1) {
				topProdutosFiltrados.add(topProdutos.get(segundo));				
			}
			if(terceiro!=-1) {
				topProdutosFiltrados.add(topProdutos.get(terceiro));				
			}
			return topProdutosFiltrados;
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}


}
