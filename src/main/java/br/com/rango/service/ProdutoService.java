package br.com.rango.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.inject.Inject;
import br.com.rango.dao.ProdutoDAO;
import br.com.rango.entities.Estabelecimento;
import br.com.rango.entities.Produto;
import br.com.rango.entities.TipoProduto;
import br.com.rango.util.TransacionalCdi;

public class ProdutoService implements Serializable{	
	
	private static final long serialVersionUID = 7959932588784011406L;
	
	@Inject
	private ProdutoDAO produtoDAO;	

	public ProdutoDAO getProdutoDAO() {
		return produtoDAO;
	}
	public void setProdutoDAO(ProdutoDAO produtoDAO) {
		this.produtoDAO = produtoDAO;
	}
	
	@TransacionalCdi
	public String cadastrarProduto(Produto produto) throws Exception {
		try {
			if(buscarProdutoPorNome(produto)==null) {				
				produtoDAO.save(produto);				
				return "Produto cadastrado com sucesso!";			
			}
			else {
				throw new Exception("Já existe uma produto cadastrado com esse nome");
			}			
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}		
	}

	public Collection<Produto> buscarTodosProdutos(Estabelecimento estabelecimento) throws Exception {
		try {
			List<Produto> produtos = produtoDAO.getAll(estabelecimento);		
			if(produtos.size()>0) {
				return produtos;
			}
			else{
				return new ArrayList<Produto>();
			}			
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}		
	}
	
	public Collection<Produto> buscarTodosPorTipo(TipoProduto tipo) throws Exception {
		try {
			List<Produto> produtos = produtoDAO.getAllByType(tipo);		
			if(produtos.size()>0) {
				return produtos;
			}
			else{
				return new ArrayList<Produto>();
			}			
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}		
	}

	public Produto buscarProdutoPorNome(Produto produto) throws Exception {
		try {
			return produtoDAO.getByName(produto);
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}		
	}

	@TransacionalCdi
	public String atualizarProduto(Produto produto) throws Exception {
		try {
			if(validarAtualizacao(produto)) {			
				produtoDAO.update(produto);			
				return "Produto atualizado com sucesso!";
			}
			else {
				throw new Exception("Já existe uma mesa com esse número!");
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}		
	}

	@TransacionalCdi
	public String removerProduto(Produto produto) throws Exception {
		try {
			produtoDAO.delete(produto);		
			return "Produto removido com sucesso!";			
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}		
	}
	
	private boolean validarAtualizacao(Produto produto) throws Exception {
		Produto produtoBusca = buscarProdutoPorNome(produto);
		if(produtoBusca==null) {
			return true;
		}
		else if (produtoBusca!=null && produtoBusca.getId()==produto.getId()) {
			return true;
		}
		return false;
	}
}
