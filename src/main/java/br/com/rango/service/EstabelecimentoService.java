package br.com.rango.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import br.com.rango.dao.EstabelecimentoDAO;
import br.com.rango.entities.Estabelecimento;
import br.com.rango.util.TransacionalCdi;

public class EstabelecimentoService implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	EstabelecimentoDAO estabelecimentoDAO;	


	public EstabelecimentoDAO getEstabelecimentoDAO() {
		return estabelecimentoDAO;
	}
	public void setEstabelecimentoDAO(EstabelecimentoDAO estabelecimentoDAO) {
		this.estabelecimentoDAO = estabelecimentoDAO;
	}

	@TransacionalCdi
	public String cadastrarEstabelecimento(Estabelecimento estabelecimento) throws Exception {	
		try {
			if(estabelecimento.getNome().length()>1 && buscarEstabelecimentoPorNome(estabelecimento)==null) {			
				estabelecimentoDAO.save(estabelecimento);			
				return "Estabelecimento cadastrado com sucesso!";
			}
			else {
				throw new Exception("Este nome é inválido ou já existe um estabelecimento cadastrado com esse nome");
			}			
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}

	}

	private Estabelecimento buscarEstabelecimentoPorNome(Estabelecimento estabelecimento) throws Exception {	
		try {
			return estabelecimentoDAO.getByName(estabelecimento);			
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}				
	}

	public List<Estabelecimento> buscarTodosEstabelecimentos() throws Exception {
		try {
			List<Estabelecimento> estabelecimentos = estabelecimentoDAO.getAll();		
			if(estabelecimentos.size()>0) {
				return estabelecimentos;
			}
			else{
				return new ArrayList<Estabelecimento>();
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}

	}

	public Estabelecimento buscarEstabelecimentoPorID(Long id) throws Exception {
		try {
			return estabelecimentoDAO.getByID(id);
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}	
	}

	@TransacionalCdi
	public String atualizarEstabelecimeto(Estabelecimento estabelecimento) throws Exception{
		try {
			if(validarAtualizacao(estabelecimento)) {			
				estabelecimentoDAO.update(estabelecimento);			
				return "Estabelecimento atualizado com sucesso!";
			}
			else {
				throw new Exception("Já existe um estabelecimento cadastrado com esse nome!");
			}			
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}		
	}

	@TransacionalCdi
	public String removerEstabelecimento(Estabelecimento estabelecimento) throws Exception {	
		try {
			estabelecimentoDAO.delete(estabelecimento);		
			return "Estabelecimento removido com sucesso!";	
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
						
	}

	private boolean validarAtualizacao(Estabelecimento estabelecimento) throws Exception {
		Estabelecimento estabelecimentoBusca = buscarEstabelecimentoPorNome(estabelecimento);
		if(estabelecimentoBusca==null) {
			return true;
		}
		else if (estabelecimentoBusca!=null && estabelecimentoBusca.getId()==estabelecimento.getId()) {
			return true;
		}
		return false;	
	}

}
