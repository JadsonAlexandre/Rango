package br.com.rango.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;

@Entity
public class Estabelecimento implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idEstabelecimento;	
	
	private Funcionario gerente;
	
	@OneToMany(cascade = {CascadeType.REFRESH,CascadeType.REMOVE}, mappedBy="estabelecimento")
	private Collection<Funcionario> funcionarios;
	
	@OneToMany(cascade = {CascadeType.REFRESH,CascadeType.REMOVE}, mappedBy="estabelecimento")
	private Collection<Mesa> mesas;
	
	@OneToMany(cascade = {CascadeType.REFRESH,CascadeType.REMOVE}, mappedBy="estabelecimento")
	private Collection<Produto> produtos;
	
	@OneToMany(cascade = {CascadeType.REFRESH,CascadeType.REMOVE}, mappedBy="estabelecimento")
	private Collection<Pedido> pedidos;
	
	@OneToMany(cascade = {CascadeType.REFRESH,CascadeType.REMOVE}, mappedBy="estabelecimento")
	private Collection<TipoProduto> tipoProduto;
	
	@OneToMany(cascade = {CascadeType.REFRESH,CascadeType.REMOVE}, mappedBy="estabelecimento")
	private Collection<ItemPedido> itemPedido;
	
	@OneToOne(cascade = {CascadeType.REFRESH,CascadeType.REMOVE})	
	private RegrasNegocio regrasNegocio;
	
	@Column(unique=true)
	private String nome;
	
	private Long telefone;	
	
	private boolean aberto;	
	
	private LatitudeLongitude latitudeLongitude;	
	
	public Long getId() {
		return idEstabelecimento;
	}
	public void setId(Long idEstabelecimento) {
		this.idEstabelecimento = idEstabelecimento;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}	
	public Long getTelefone() {
		return telefone;
	}
	public void setTelefone(Long telefone) {
		this.telefone = telefone;
	}
	public boolean isAberto() {
		return aberto;
	}
	public void setAberto(boolean aberto) {
		this.aberto = aberto;
	}
	@Embedded
	public LatitudeLongitude getLatitudeLongitude() {
		return latitudeLongitude;
	}
	public void setLatitudeLongitude(LatitudeLongitude latitudeLongitude) {
		this.latitudeLongitude = latitudeLongitude;
	}	
	public Collection<Funcionario> getFuncionarios() {
		return funcionarios;
	}
	public void setFuncionarios(Collection<Funcionario> funcionarios) {
		this.funcionarios = funcionarios;
	}
	public Collection<Mesa> getMesas() {
		return mesas;
	}
	public void setMesas(Collection<Mesa> mesas) {
		this.mesas = mesas;
	}
	public Collection<Produto> getProdutos() {
		return produtos;
	}
	public void setProdutos(Collection<Produto> produtos) {
		this.produtos = produtos;
	}
	public Collection<Pedido> getPedidos() {
		return pedidos;
	}
	public void setPedidos(Collection<Pedido> pedidos) {
		this.pedidos = pedidos;
	}
	public Collection<TipoProduto> getTipoProduto() {
		return tipoProduto;
	}
	public void setTipoProduto(Collection<TipoProduto> tipoProduto) {
		this.tipoProduto = tipoProduto;
	}
	public RegrasNegocio getRegrasNegocio() {
		return regrasNegocio;
	}
	public void setRegrasNegocio(RegrasNegocio regrasNegocio) {
		this.regrasNegocio = regrasNegocio;
	}	
	public Funcionario getGerente() {
		return gerente;
	}
	public void setGerente(Funcionario gerente) {
		this.gerente = gerente;
	}	
	public Collection<ItemPedido> getItemPedido() {
		return itemPedido;
	}
	public void setItemPedido(Collection<ItemPedido> itemPedido) {
		this.itemPedido = itemPedido;
	}
	
	public Estabelecimento() {
	}
	
	public Estabelecimento(String nome, Long telefone) {		
		this.nome = nome;		
		this.telefone = telefone;		
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idEstabelecimento == null) ? 0 : idEstabelecimento.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Estabelecimento other = (Estabelecimento) obj;
		if (idEstabelecimento == null) {
			if (other.idEstabelecimento != null)
				return false;
		} else if (!idEstabelecimento.equals(other.idEstabelecimento))
			return false;
		return true;
	}
	
	public Estabelecimento clone() {
		Estabelecimento clone = new Estabelecimento();
		clone.setAberto(aberto);
		clone.setFuncionarios(funcionarios);
		clone.setGerente(gerente);
		clone.setId(idEstabelecimento);
		clone.setLatitudeLongitude(latitudeLongitude);
		clone.setMesas(mesas);
		clone.setNome(nome);
		clone.setPedidos(pedidos);
		clone.setProdutos(produtos);
		clone.setRegrasNegocio(regrasNegocio);
		clone.setTelefone(telefone);
		clone.setTipoProduto(tipoProduto);
		return clone;
	}
}
