package br.com.rango.entities;

import java.io.Serializable;

import javax.persistence.*;

@Entity
public class ItemPedido implements Serializable{
	
	private static final long serialVersionUID = -3470033270817943776L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private Integer quantidade;

	private boolean entregue;
	
	@Enumerated(EnumType.STRING)
	private StatusItem status;

	@JoinColumn(name = "idPedido", referencedColumnName = "idPedido")
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private Pedido pedido;

	@JoinColumn(name = "idProduto", referencedColumnName = "idProduto")
	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	private Produto produto;
	
	@JoinColumn(name = "idEstabelecimento", referencedColumnName = "idEstabelecimento")
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private Estabelecimento estabelecimento;
	
	public boolean isEmPreparo() {
		if(status == StatusItem.EM_PREPARO) {
			return true;
		}
		return false;
	}
	
	public boolean isNaFila() {
		if(status == StatusItem.NA_FILA) {
			return true;
		}
		return false;
	}
	
	public boolean isPronto() {
		if(status == StatusItem.PRONTO) {
			return true;
		}
		return false;
	}
	
	public boolean isEntregavel() {
		if(isPronto()&&!isEntregue()) {
			return true;
		}
		return false;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Integer getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}
	public boolean isEntregue() {
		return entregue;
	}
	public void setEntregue(boolean entregue) {
		this.entregue = entregue;
	}
	public Produto getProduto() {
		return produto;
	}
	public void setProduto(Produto produto) {
		this.produto = produto;
	}	
	public Pedido getPedido() {
		return pedido;
	}
	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}	
	public Estabelecimento getEstabelecimento() {
		return estabelecimento;
	}
	public void setEstabelecimento(Estabelecimento estabelecimento) {
		this.estabelecimento = estabelecimento;
	}	
	public StatusItem getStatus() {
		return status;
	}
	public void setStatus(StatusItem status) {
		this.status = status;
	}
	
	public ItemPedido() {
		
	}
	public ItemPedido(Integer quantidade, boolean entregue, Produto produto, Pedido pedido) {
		
		this.quantidade = quantidade;
		this.entregue = entregue;
		this.produto = produto;
		this.pedido = pedido;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (entregue ? 1231 : 1237);
		result = prime * result + ((pedido == null) ? 0 : pedido.hashCode());
		result = prime * result + ((produto == null) ? 0 : produto.hashCode());
		result = prime * result + ((quantidade == null) ? 0 : quantidade.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemPedido other = (ItemPedido) obj;
		if (entregue != other.entregue)
			return false;
		if (pedido == null) {
			if (other.pedido != null)
				return false;
		} else if (!pedido.equals(other.pedido))
			return false;
		if (produto == null) {
			if (other.produto != null)
				return false;
		} else if (!produto.equals(other.produto))
			return false;
		if (quantidade == null) {
			if (other.quantidade != null)
				return false;
		} else if (!quantidade.equals(other.quantidade))
			return false;
		return true;
	}
	
	
	
	

}
