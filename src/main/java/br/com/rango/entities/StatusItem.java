package br.com.rango.entities;

public enum StatusItem {
	NA_FILA("Na Fila"), EM_PREPARO("Em Preparo"), PRONTO("Pronto");
	
	private String descricao;
	
	private StatusItem(String descricao) { 
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
	
	

}
