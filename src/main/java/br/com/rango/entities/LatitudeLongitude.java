package br.com.rango.entities;

import javax.persistence.Embeddable;

@Embeddable
public class LatitudeLongitude {
	
	private double latitude;
	private double longitude;
	
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}	
	
	public LatitudeLongitude() {
		
	}
	public LatitudeLongitude(double latitude, double longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
	}
	
}
