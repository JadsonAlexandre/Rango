package br.com.rango.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Mesa implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 701002225852447420L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idMesa;
	
	private Long numero;
	
	@JoinColumn(name = "idEstabelecimento", referencedColumnName = "idEstabelecimento")
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private Estabelecimento estabelecimento;
	
	private boolean ocupada;

	private boolean externa;

	private Integer capacidade;
	
	
	public Long getId() {
		return idMesa;
	}
	public void setId(Long idMesa) {
		this.idMesa = idMesa;
	}
	public Long getNumero() {
		return numero;
	}
	public void setNumero(Long numero) {
		this.numero = numero;
	}
	public boolean isOcupada() {
		return ocupada;
	}
	public void setOcupada(boolean ocupada) {
		this.ocupada = ocupada;
	}
	public boolean isExterna() {
		return externa;
	}
	public void setExterna(boolean externa) {
		this.externa = externa;
	}
	public Integer getCapacidade() {
		return capacidade;
	}
	public void setCapacidade(Integer capacidade) {
		this.capacidade = capacidade;
	}	
	public Estabelecimento getEstabelecimento() {
		return estabelecimento;
	}
	public void setEstabelecimento(Estabelecimento estabelecimento) {
		this.estabelecimento = estabelecimento;
	}	
	public Mesa() {
		
	}
	public Mesa(long numero, Estabelecimento estabelecimento, boolean ocupada, boolean externa, int capacidade) {
		this.estabelecimento = estabelecimento;
		this.numero = numero;
		this.ocupada = ocupada;
		this.externa = externa;
		this.capacidade = capacidade;
	}
	
	public String getColor() {
		if(!this.ocupada) {
			return "#00A859";
		}
		return "#D12034";
	}
	
	public Mesa clone() {
		Mesa clone = new Mesa();
		clone.setCapacidade(capacidade);
		clone.setEstabelecimento(estabelecimento);
		clone.setExterna(externa);
		clone.setId(idMesa);
		clone.setNumero(numero);
		clone.setOcupada(ocupada);		
		return clone;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((estabelecimento == null) ? 0 : estabelecimento.hashCode());
		result = prime * result + ((numero == null) ? 0 : numero.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Mesa other = (Mesa) obj;
		if (estabelecimento == null) {
			if (other.estabelecimento != null)
				return false;
		} else if (!estabelecimento.equals(other.estabelecimento))
			return false;
		if (numero == null) {
			if (other.numero != null)
				return false;
		} else if (!numero.equals(other.numero))
			return false;
		return true;
	}
	
	
	
	
	

}
