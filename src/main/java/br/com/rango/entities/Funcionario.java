package br.com.rango.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Funcionario implements Serializable{
	
	private static final long serialVersionUID = 1L;	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idFuncionario;
	
	@Column(unique=true)	
	private String login;
	
	private String senha;
	
	private String nome;

	@Column(unique=true)
	private String cpf;
	
	@Enumerated(EnumType.STRING)
	private TipoFuncionario tipoFuncionario;
	
	@JoinColumn(name = "idEstabelecimento", referencedColumnName = "idEstabelecimento")
	@ManyToOne(cascade=CascadeType.REFRESH, optional = false, fetch = FetchType.LAZY)
	private Estabelecimento estabelecimento;
	
	@OneToMany(fetch = FetchType.LAZY,mappedBy="garcom")
	private List<Pedido> pedidos;
	
	private String email;
	
	private Long telefone;
	
	
	public Long getIdFuncionario() {
		return idFuncionario;
	}
	public void setIdFuncionario(Long idFuncionario) {
		this.idFuncionario = idFuncionario;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public TipoFuncionario getTipoFuncionario() {
		return tipoFuncionario;
	}
	public void setTipoFuncionario(TipoFuncionario tipoFuncionario) {
		this.tipoFuncionario = tipoFuncionario;
	}	
	public Estabelecimento getEstabelecimento() {
		return estabelecimento;
	}
	public void setEstabelecimento(Estabelecimento estabelecimento) {
		this.estabelecimento = estabelecimento;
	}		
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Long getTelefone() {
		return telefone;
	}
	public void setTelefone(Long telefone) {
		this.telefone = telefone;
	}	
	public List<Pedido> getPedidos() {
		return pedidos;
	}
	public void setPedidos(List<Pedido> pedidos) {
		this.pedidos = pedidos;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idFuncionario == null) ? 0 : idFuncionario.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Funcionario other = (Funcionario) obj;
		if (idFuncionario == null) {
			if (other.idFuncionario != null)
				return false;
		} else if (!idFuncionario.equals(other.idFuncionario))
			return false;
		return true;
	}
	
	public Funcionario() {
	}
	
	public Funcionario(String login, String senha, String nome, String cpf, TipoFuncionario tipoFuncionario) {		
		this.login = login;
		this.senha = senha;
		this.nome = nome;
		this.cpf = cpf;
		this.tipoFuncionario = tipoFuncionario;
	}
	
	public Funcionario clone() {
		Funcionario clone = new Funcionario();
		clone.setIdFuncionario(idFuncionario);;
		clone.setLogin(login);
		clone.setCpf(cpf);
		clone.setEmail(email);
		clone.setEstabelecimento(estabelecimento);
		clone.setSenha(senha);
		clone.setTelefone(telefone);
		clone.setNome(nome);
		clone.setTipoFuncionario(tipoFuncionario);		
		return clone;
	}
	
	
	
	
}
