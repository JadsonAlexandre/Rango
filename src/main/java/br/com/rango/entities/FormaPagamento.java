package br.com.rango.entities;

public enum FormaPagamento {
	DINHEIRO("Dinheiro"), CARTAO("Cartão"), CARTAO_DINHEIRO("Cartão e Dinheiro");
	
	private String nome;
	
	private FormaPagamento(String nome){
		this.nome = nome;		
	}

	public String getNome() {
		return nome;
	}	
}
