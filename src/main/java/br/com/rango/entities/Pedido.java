package br.com.rango.entities;

import java.util.Date;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;

@Entity
public class Pedido implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idPedido;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dataAbertura;

	private String nomeCliente;

	private Float taxa;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dataFechamento;	

	@OneToMany(mappedBy="pedido", fetch = FetchType.EAGER, orphanRemoval=true)
	private Collection<ItemPedido> itens;	

	@JoinColumn(name = "idMesa", referencedColumnName = "idMesa")
	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	private Mesa mesa;	

	@JoinColumn(name = "idFuncionario", referencedColumnName = "idFuncionario")
	@ManyToOne(cascade=CascadeType.REFRESH, optional = false, fetch = FetchType.LAZY)
	private Funcionario garcom;

	@JoinColumn(name = "idEstabelecimento", referencedColumnName = "idEstabelecimento")
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private Estabelecimento estabelecimento;

	private Float valorProdutos;
	private Float valorDesconto;
	private FormaPagamento formaPagamento;
	private Float valorPago;	

	public float subTotal() {
		float resultado = (float) 0;
		if(itens!=null) {
			for(ItemPedido item: itens) {
				if(item.isEntregue()) {
					resultado+=item.getProduto().getValorVenda().floatValue()*(item.getQuantidade());
				}
			}				
		}
		return resultado;
	}

	public Long getIdPedido() {
		return idPedido;
	}
	public void setIdPedido(Long idPedido) {
		this.idPedido = idPedido;
	}
	public Date getDataAbertura() {
		return dataAbertura;
	}
	public void setDataAbertura(Date dataAbertura) {
		this.dataAbertura = dataAbertura;
	}
	public String getNomeCliente() {
		return nomeCliente;
	}
	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}
	public Float getTaxa() {
		return taxa;
	}
	public void setTaxa(Float taxa) {
		this.taxa = taxa;
	}
	public Date getDataFechamento() {
		return dataFechamento;
	}
	public void setDataFechamento(Date dataFechamento) {
		this.dataFechamento = dataFechamento;
	}	
	public Collection<ItemPedido> getItens() {
		return itens;
	}
	public void setItens(Collection<ItemPedido> itens) {
		this.itens = itens;
	}
	public Float getValorProdutos() {		
		return valorProdutos;
	}
	public void setValorProdutos(Float valorProdutos) {
		this.valorProdutos = valorProdutos;
	}
	public Float getValorDesconto() {
		return valorDesconto;
	}
	public void setValorDesconto(Float valorDesconto) {
		this.valorDesconto = valorDesconto;
	}
	public FormaPagamento getFormaPagamento() {
		return formaPagamento;
	}
	public void setFormaPagamento(FormaPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}
	public Float getValorPago() {
		return valorPago;
	}
	public void setValorPago(Float valorPago) {
		this.valorPago = valorPago;
	}
	public Mesa getMesa() {
		return mesa;
	}
	public void setMesa(Mesa mesa) {
		this.mesa = mesa;
	}	
	public float valorTroco() {
		return (float) 0;
	}	
	public Estabelecimento getEstabelecimento() {
		return estabelecimento;
	}
	public void setEstabelecimento(Estabelecimento estabelecimento) {
		this.estabelecimento = estabelecimento;
	}
	public Funcionario getGarcom() {
		return garcom;
	}
	public void setGarcom(Funcionario garcom) {
		this.garcom = garcom;
	}
	public Pedido() {

	}
	public Pedido(Estabelecimento estabelecimento, String nomeCliente, Float taxa, Mesa mesa) {
		this.estabelecimento = estabelecimento;
		this.dataAbertura = new Date(System.currentTimeMillis());
		this.nomeCliente = nomeCliente;
		this.taxa = taxa;
		this.mesa = mesa;

	}

	public Pedido clone() {
		Pedido clone = new Pedido();
		clone.setDataAbertura(dataAbertura);
		clone.setDataFechamento(dataFechamento);
		clone.setEstabelecimento(estabelecimento);
		clone.setFormaPagamento(formaPagamento);
		clone.setIdPedido(idPedido);
		clone.setItens(itens);
		clone.setMesa(mesa);
		clone.setNomeCliente(nomeCliente);
		clone.setTaxa(taxa);
		clone.setValorDesconto(valorDesconto);
		clone.setValorPago(valorPago);
		clone.setValorProdutos(valorProdutos);		
		return clone;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((mesa == null) ? 0 : mesa.hashCode());
		result = prime * result + ((nomeCliente == null) ? 0 : nomeCliente.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pedido other = (Pedido) obj;
		if (mesa == null) {
			if (other.mesa != null)
				return false;
		} else if (!mesa.equals(other.mesa))
			return false;
		if (nomeCliente == null) {
			if (other.nomeCliente != null)
				return false;
		} else if (!nomeCliente.equals(other.nomeCliente))
			return false;
		return true;
	}





}
