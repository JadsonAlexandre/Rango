package br.com.rango.entities;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Collection;
import java.util.Locale;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;


@Entity
public class Produto {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idProduto;

	private String nome;

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	private TipoProduto tipo;

	@Column(scale=2,precision=12)
	private BigDecimal valorCusto;

	@Column(scale=2,precision=12)
	private BigDecimal valorVenda;

	private Integer quantidade;

	private boolean daCasa;

	@JoinColumn(name = "idEstabelecimento", referencedColumnName = "idEstabelecimento")
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private Estabelecimento estabelecimento;
	
	@OneToMany(mappedBy="produto", fetch = FetchType.LAZY, orphanRemoval=true)
	private Collection<ItemPedido> itens; 
	
	private String URLFoto;
	
	
	public boolean isDisponivel() {
		if(!this.daCasa && this.quantidade==0) {
			return false;
		}
		return true;
	}
	
	public Long getId() {
		return idProduto;
	}
	public void setId(Long id) {
		this.idProduto = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public BigDecimal getValorCusto() {
		return valorCusto;
	}
	public void setValorCusto(BigDecimal valorCusto) {
		this.valorCusto = valorCusto;
	}
	public BigDecimal getValorVenda() {
		return valorVenda;
	}
	public void setValorVenda(BigDecimal valorVenda) {
		this.valorVenda = valorVenda;
	}
	public Integer getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}
	public boolean isDaCasa() {
		return daCasa;
	}
	public void setDaCasa(boolean daCasa) {
		this.daCasa = daCasa;
	}
	public String getURLFoto() {
		return URLFoto;
	}
	public void setURLFoto(String URLFoto) {
		this.URLFoto = URLFoto;
	}
	public TipoProduto getTipo() {
		return tipo;
	}
	public void setTipo(TipoProduto tipo) {
		this.tipo = tipo;
	}	
	public Estabelecimento getEstabelecimento() {
		return estabelecimento;
	}
	public void setEstabelecimento(Estabelecimento estabelecimento) {
		this.estabelecimento = estabelecimento;
	}
	
	public Produto() {
		
	}
	public Produto(String nome, Estabelecimento estabelecimento, TipoProduto tipo, BigDecimal valorCusto, BigDecimal valorVenda, int quantidade, boolean daCasa, String URLFoto) {
		this.nome = nome;
		this.estabelecimento = estabelecimento;
		this.tipo = tipo;
		this.valorCusto = valorCusto;
		this.valorVenda = valorVenda;
		this.quantidade = quantidade;
		this.daCasa = daCasa;
		this.URLFoto = URLFoto;
	}
	
	public float duasCasasDecimais(float i) {
		NumberFormat numberFormat = NumberFormat.getInstance(new Locale("en", "US"));
		numberFormat.setMaximumFractionDigits(2);		
        return Float.valueOf((numberFormat.format(i)));		
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (idProduto ^ (idProduto >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Produto other = (Produto) obj;
		if (idProduto != other.idProduto)
			return false;
		return true;
	}
	
	public Produto clone() {
		Produto clone = new Produto();
		clone.setDaCasa(daCasa);
		clone.setEstabelecimento(estabelecimento);
		clone.setId(idProduto);
		clone.setNome(nome);
		clone.setQuantidade(quantidade);
		clone.setTipo(tipo);
		clone.setURLFoto(URLFoto);
		clone.setValorCusto(valorCusto);
		clone.setValorVenda(valorVenda);		
		return clone;
	}
	
	
	
	

}
