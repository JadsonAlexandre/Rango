package br.com.rango.entities;

public enum TipoFuncionario {
	GERENTE("Gerente"),GARCOM("Garçom"),COZINHEIRO("Cozinheiro"),CAIXA("Caixa");
	
	public String descricao;
	
	private TipoFuncionario(String descricao){
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
	
	
}
