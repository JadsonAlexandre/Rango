package br.com.rango.entities;

import javax.persistence.*;

@Entity
public class TipoProduto {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	private String tipo;		
	
	@JoinColumn(name = "idEstabelecimento", referencedColumnName = "idEstabelecimento")
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private Estabelecimento estabelecimento;

	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}	
	public Estabelecimento getEstabelecimento() {
		return estabelecimento;
	}
	public void setEstabelecimento(Estabelecimento estabelecimento) {
		this.estabelecimento = estabelecimento;
	}
	public TipoProduto() {		
	}

	public TipoProduto(String tipo, Estabelecimento estabelecimento) {
		this.tipo = tipo;
		this.estabelecimento = estabelecimento;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoProduto other = (TipoProduto) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	public TipoProduto clone() {
		TipoProduto clone = new TipoProduto();
		clone.setId(id);
		clone.setTipo(tipo);
		clone.setEstabelecimento(estabelecimento);
		return clone;
	}
	
}
