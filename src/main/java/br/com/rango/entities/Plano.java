package br.com.rango.entities;

public enum Plano {
	OFFLINE("Offline", 50), ONLINE("Online", 100), ONLINE_DELIVERY("Delivery", 150);
	
	String nome;
	double preco;
	
	private Plano(String nome, double preco){
		this.nome = nome;
		this.preco = preco;
	}

	public String getNome() {
		return nome;
	}

	public double getPreco() {
		return preco;
	}	
}
