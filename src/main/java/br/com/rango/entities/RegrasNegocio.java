package br.com.rango.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class RegrasNegocio implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;

	private int quantMinProduto;

	private float valorMesaPremium;	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getQuantMinProduto() {
		return quantMinProduto;
	}
	public void setQuantMinProduto(int quantMinProduto) {
		this.quantMinProduto = quantMinProduto;
	}
	public float getValorMesaPremium() {
		return valorMesaPremium;
	}
	public void setValorMesaPremium(float valorMesaPremium) {
		this.valorMesaPremium = valorMesaPremium;
	}		
	
	public RegrasNegocio() {
		
	}
	
	public RegrasNegocio(int quantMinProduto, float valorMesaPremium) {
		this.quantMinProduto = quantMinProduto;
		this.valorMesaPremium = valorMesaPremium;		
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RegrasNegocio other = (RegrasNegocio) obj;
		if (id != other.id)
			return false;
		return true;
	}	

}
