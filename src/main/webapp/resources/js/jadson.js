jQuery(function($){	
	$("input.mascara-telefone").mask("(00) 00000-0000");
	$(".mascara-telefone").mask("(00) 00000-0000");
	$("input.mascara-cpf").mask("000.000.000-00");
	$("input.mascara-numero").mask("000000");
	$("input.mascara-numero-mesa").mask("000");	
	$("input.mascara-real").maskMoney({
		prefix: "R$",
		decimal: ",",
		thousands: "."
	});
	$(".mascara-real").maskMoney({
		prefix: "R$",
		decimal: ",",
		thousands: "."
	});
});

jQuery(function($){	
	$("input.mascara-numero-mesa").mask("000");	
});



